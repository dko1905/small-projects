#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

void play(void);

int main(void)
{
	srand(time(NULL));

	for (size_t n = 0; n < 10000; ++n)
		play();

	return 0;
}

void play(void)
{
	int correct = 0, pick = 0, incorrect = 0;
	bool switched = false;

	/* Monty picks correct door. */
	correct = rand() % 3;

	/* Player picks random door. */
	pick = rand() % 3;

	/* Monty picks incorrect door. */
	if (pick == correct) {
		incorrect = (pick + ((rand() % 2) + 1)) % 3;
	} else if (pick == (correct + 1) % 3) {
		incorrect = (correct + 2) % 3;
	} else {
		incorrect = (correct + 1) % 3;
	}

	//printf("%i %i %i\n", correct, pick, incorrect);

	/* Does player switch? */
	if (rand() % 2 == 0) {
		if (pick == (incorrect + 1) % 3)
			pick = (incorrect + 2) % 3;
		else
			pick = (incorrect + 1) % 3;
		switched = true;
	}

	/* Print results. */
	if (pick == correct) {
		if (switched)
			printf("1");
		else
			printf("2");
	} else {
		printf("0");
	}

	printf("\n");
}

