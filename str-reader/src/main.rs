use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    io::{self, stdout, BufRead, Write},
    ops::{Index, RangeBounds},
    time::Instant,
};

use anyhow::{anyhow, bail, Context, Error};
use compact_str::CompactString;
use istring::IString;
use size_of::SizeOf;
use smol_str::SmolStr;
use ustr::Ustr;

fn main() {
    let stdin = std::io::stdin();
    let mut stdin = stdin.lock();

    let mut state: HashMap<String, SmolStr> = HashMap::new();
    loop {
        let mut line = String::new();

        print!("-> ");
        io::stdout().flush().unwrap();

        match stdin.read_line(&mut line) {
            Ok(count) => {
                if count == 0 {
                    break; // EOF
                }
                let ts = Instant::now();
                match run(&line.trim(), &mut state) {
                    Err(err) => eprintln!("{}", err),
                    Ok(_) => (),
                }
                println!("{} ms", (Instant::now() - ts).as_millis());
            }
            Err(err) => {
                eprintln!("{}", err);
                break;
            }
        }
    }
}

fn run<T: Display + Debug + Len + Sliceable + From<String>>(
    line: &str,
    state: &mut HashMap<String, T>,
) -> Result<(), Error> {
    let (word, rest) = line.split_once(" ").unwrap_or((line, ""));

    match word {
        "vars" => {
            println!("Vars:");
            for (k, v) in state.iter() {
                println!("{}=len({})", k, v.len());
            }
        }
        "set" => {
            let (k, v) = rest
                .split_once(" ")
                .ok_or(anyhow!("invalid usage of 'set'"))?;

            state.insert(k.into(), v.to_owned().into());
        }
        "get" => {
            let k = rest;

            match state.get(k) {
                None => println!("variable \"{}\" does not exist", k),
                Some(val) => println!("{}", val),
            }
        }
        "cat" => {
            let (k, path) = rest
                .split_once(" ")
                .ok_or(anyhow!("invalid usage of 'cat'"))?;

            let data = std::fs::read_to_string(path).context(format!("file {} not found", path))?;
            state.insert(k.into(), data.into());
        }
        "len" => {
            let k = rest;

            match state.get(k) {
                None => println!("variable \"{}\" does not exist", k),
                Some(val) => println!("len of {} is {}", k, val.len()),
            }
        }
        "head" => {
            let k = rest;

            match state.get(k) {
                None => println!("variable \"{}\" does not exist", k),
                Some(val) => {
                    println!(
                        "first 20 chars: \"{}\"",
                        &val.slice_first_to_str(20)
                    );
                }
            }
        }
        "tail" => {
            let k = rest;

            match state.get(k) {
                None => println!("variable \"{}\" does not exist", k),
                Some(val) => {
                    println!(
                        "last 20 chars: \"{}\"",
                        &val.slice_last_to_str(20)
                    );
                }
            }
        }
        "del" => {
            let k = rest;

            if !state.contains_key(k) {
                println!("variable \"{}\" does not exist", k)
            }

            state.remove(k);
        }
        "memsize" => {
            let k = rest;

            match state.get(k) {
                None => println!("variable \"{}\" does not exist", k),
                Some(val) => {
                    println!(
                        "mem size: \"{}\"",
                        size_of::size_of_values([&val.as_str() as &dyn SizeOf]).total_bytes()
                    );
                }
            }
        }
        _ => {
            bail!("invalid command: '{}'", word);
        }
    }

    Ok(())
}

trait Len {
    fn len(&self) -> usize;
}

impl Len for String {
    fn len(&self) -> usize {
        self.len()
    }
}

impl Len for CompactString {
    fn len(&self) -> usize {
        self.len()
    }
}

impl Len for SmolStr {
    fn len(&self) -> usize {
        self.len()
    }
}

impl Len for IString {
    fn len(&self) -> usize {
        // Missing .len() on IString...
        self.as_str().len()
    }
}

impl Len for Ustr {
    fn len(&self) -> usize {
        self.len()
    }
}


trait Sliceable {
    fn as_str<'a>(&'a self) -> &'a str;
    fn slice_as_str<'a>(&'a self, start: usize, end: usize) -> &'a str;
    fn slice_first_to_str<'a>(&'a self, count: usize) -> &'a str;
    fn slice_last_to_str<'a>(&'a self, count: usize) -> &'a str;
}


impl Sliceable for String {
    fn slice_as_str<'a>(&'a self, start: usize, end: usize) -> &'a str{
        return &self[start..end];
    }
    fn slice_first_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[..std::cmp::min(count, self.len())];
    }
    fn slice_last_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[self.len() - std::cmp::min(count, self.len())..];
    }
    
    fn as_str<'a>(&'a self) -> &'a str {
        self.as_str()
    }
}

impl Sliceable for CompactString {
    fn slice_as_str<'a>(&'a self, start: usize, end: usize) -> &'a str{
        return &self[start..end];
    }
    fn slice_first_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[..std::cmp::min(count, self.len())];
    }
    fn slice_last_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[self.len() - std::cmp::min(count, self.len())..];
    }
    
    fn as_str<'a>(&'a self) -> &'a str {
        self.as_str()
    }
}

impl Sliceable for SmolStr {
    fn slice_as_str<'a>(&'a self, start: usize, end: usize) -> &'a str{
        return &self[start..end];
    }
    fn slice_first_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[..std::cmp::min(count, self.len())];
    }
    fn slice_last_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[self.len() - std::cmp::min(count, self.len())..];
    }
    fn as_str<'a>(&'a self) -> &'a str {
        self.as_str()
    }
}

impl Sliceable for IString {
    fn slice_as_str<'a>(&'a self, start: usize, end: usize) -> &'a str{
        return &self[start..end];
    }
    fn slice_first_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[..std::cmp::min(count, self.len())];
    }
    fn slice_last_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[self.len() - std::cmp::min(count, self.len())..];
    }
    fn as_str<'a>(&'a self) -> &'a str {
        self.as_str()
    }
}

impl Sliceable for Ustr {
    fn slice_as_str<'a>(&'a self, start: usize, end: usize) -> &'a str{
        return &self[start..end];
    }
    fn slice_first_to_str<'a>(&'a self, count: usize) -> &'a str {
        println!("inner ptr: {:#?}", self.as_ptr());
        return &self[..std::cmp::min(count, self.len())];
    }
    fn slice_last_to_str<'a>(&'a self, count: usize) -> &'a str {
        return &self[self.len() - std::cmp::min(count, self.len())..];
    }
    fn as_str<'a>(&'a self) -> &'a str {
        self.as_str()
    }
}
