import DiscordJS, {Intents} from 'discord.js'
import dotenv from 'dotenv'

dotenv.config()

const client = new DiscordJS.Client({
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES
	]
})

client.on('ready', () => {
	console.info('Ready!')

	client.on('messageCreate', (message) => {
		if (message.content.startsWith(',th ') && message.hasThread === false) {
			const name = message.author.toString() + ' ' + message.content.substring(message.content.indexOf(' ')+1)

			if (message.deletable === false) {
				console.error("Cannot delete message");
			}

			message.channel.send({
				content: name
			}).then(message => {
				return message.startThread({
					name: name,
					autoArchiveDuration: 60,
					reason: 'Why not?'
				}).then(thread => {
					console.log(`Create thread ${thread}`)
				})
			}).catch(err => {
				console.error(`Failed to create thread: ${err}`)
			})


		}
	})

	client.comm
})


client.login(process.env.TOKEN)
