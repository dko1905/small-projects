import { Component, createSignal, For, Match, Switch } from 'solid-js';
import { createStore } from 'solid-js/store'
import TicTacToe from './TicTacToe';

const App: Component = () => {
  const [games, setGames] = createStore<{
    id: string,
    name: string,
  }[]>([]);
  const [selected, setSelected] = createSignal<string>("tic-tac-toe");

  setGames([{
    id: 'tic-tac-toe',
    name: 'Tic-tac-toe',
  }, {
    id: 'test',
    name: 'Test',
  }]);

  return (
    <>
      <h1>Javascript games:</h1>
      <div>
        <span>Select game:</span>
        <select onChange={e => setSelected(games[e.currentTarget.selectedIndex].id)}>
          <For each={games}>
            {(game) =>
              <option>{game.name}</option>
            }
          </For>
        </select>
      </div><br />
      <Switch fallback={<p>No game selected</p>}>
        <Match when={selected() === 'tic-tac-toe'}><TicTacToe /></Match>
      </Switch>
    </>
  );
};

export default App;
