import { Component, createSignal, onMount, runWithOwner, Show } from "solid-js";

const TicTacToe: Component = () => {
  let canvas: HTMLCanvasElement;
  let ctx: CanvasRenderingContext2D;
  let grid: Array<Array<number>>;
  let [currentPlayer, setCurrentPlayer] = createSignal(1); // 1 or 2
  let [winner, setWinner] = createSignal(0); // 0, 1 or 2

  const BOARD_SIZE = 300;
  const PADDING = 20;
  const CELL_SIZE = (BOARD_SIZE - (PADDING * 4)) / 3;

  onMount(() => {
    ctx = canvas.getContext('2d');

    grid = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
    ];

    const render = function() {
      ctx.clearRect(0, 0, BOARD_SIZE, BOARD_SIZE);
      ctx.fillStyle = 'red';
      for (let j = 0; j < grid.length; j += 1) {
        const row = grid[j];
        for (let k = 0; k < row.length; k += 1) {
          const elm = row[k];

          switch (elm) {
          case 0:
            ctx.fillStyle = 'lightgray'; break;
          case 1:
            ctx.fillStyle = 'red'; break;
          case 2:
            ctx.fillStyle = 'green'; break;
          }
          ctx.fillRect(
            PADDING + (k * (CELL_SIZE + PADDING)),
            PADDING + (j * (CELL_SIZE + PADDING)),
            CELL_SIZE, CELL_SIZE);
        }
      }
    };

    const click = function(x: number, y: number) {
      if (grid[y][x] !== 0) return;
      // Put mark on grid
      grid[y][x] = currentPlayer();

      // Check if won
      for (let j = 0; j < grid.length; j += 1) {
        const row = grid[j];

        // If row is horizontally complete
        if (row.filter(c => c === currentPlayer()).reduce((p, c) => p + c, 0) === row.length * currentPlayer()) {
          setWinner(currentPlayer());
        }
      }
      for (let k = 0; k < grid[0].length; k += 1) {
        const column = grid.map(c => c[k]);

        // If column is vertically complete
        if (column.filter(c => c === currentPlayer()).reduce((p, c) => p + c, 0) === grid.length * currentPlayer()) {
          setWinner(currentPlayer());
        }
      }

      // Requires grid.length === grid[0].length
      const leftDiagonal = grid.map((c, i) => c[i]).filter(c => c === currentPlayer());
      if (leftDiagonal.reduce((p, c) => p + c, 0) === grid.length  * currentPlayer()) {
        setWinner(currentPlayer());
      }
      const rightDiagonal = grid.map((c, i) => c[grid.length - 1 - i]).filter(c => c === currentPlayer());
      if (rightDiagonal.reduce((p, c) => p + c, 0) === grid.length  * currentPlayer()) {
        setWinner(currentPlayer());
      }

      // Switch current player
      setCurrentPlayer(currentPlayer() % 2 + 1);

      render();
    };

    const onClick = function(e: MouseEvent) {
      const [x, y] = [e.clientX - canvas.offsetLeft, e.clientY - canvas.offsetTop];

      // Ignore if already won
      if (winner() !== 0) {
        setWinner(0);
        grid = [
          [0, 0, 0],
          [0, 0, 0],
          [0, 0, 0]
        ];
        render();
        return;
      }

      for (let j = 0; j < grid.length; j += 1) {
        const row = grid[j];
        if (y > PADDING + (j * (CELL_SIZE + PADDING)) &&
            y < PADDING + (j + 1) * CELL_SIZE + j * PADDING) {
          for (let k = 0; k < row.length; k += 1) {
            if (x > PADDING + (k * (CELL_SIZE + PADDING)) &&
                x < PADDING + (k + 1) * CELL_SIZE + k * PADDING) {
              click(k, j);
            }
          }
        }
      }
    };

    canvas.onmouseup = onClick;

    render();
  });

  return (
    <div>
      <canvas ref={canvas} width={BOARD_SIZE} height={BOARD_SIZE} style="border: solid 4px skyblue;"></canvas>
      <Show when={winner() !== 0} fallback={<p>Player {currentPlayer()}'s turn</p>}>
        <p>Player {winner()} won!</p>
      </Show>
    </div>
  )
};

export default TicTacToe;
