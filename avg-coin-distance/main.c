#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

void flipx(unsigned int times, unsigned int count, bool threed);

int main(void)
{
	for (unsigned int n = 1; n <= 1000; n++) {
//		for (unsigned int i = 1; i <= 500; i++) {
//			flipx(n, i, true);
//		}
		flipx(n, 1005, false);
	}

	return 0;
}

void flipx(unsigned int times, unsigned int count, bool threed)
{
	/* TIMES to flip coin. *
	 * COUNT to avg.       */
	double total_value = 0;

	for (unsigned int n = 0; n < count; n++) {
		double value = 0;

		for (unsigned int i = times + 1; i > 1; i--) {
			if (rand() % 2 == 0)
				value++;
			else
				value--;
		}

		total_value += value;
	}

	total_value = fabs(total_value);
	total_value /= count;

	if (threed)
		printf("%u,%u,%f\n", times, count, total_value);
	else
		printf("%u, %f\n", times, total_value);
}

