import { component$ } from "@builder.io/qwik";
import { Form, routeAction$ } from "@builder.io/qwik-city";
import { uploadCsv } from "~/utils/messages";
import { tursoClient } from "~/utils/turso";

export const useUploadAction = routeAction$(async (_data, ev) => {
  // SERVER
  const data = _data as Record<string, any> as Record<string, File | null>;

  if (data.file == null || data.file.size == 0) {
    return ev.fail(400, { message: "No file provided" });
  }

  const client = tursoClient(ev);
  try {
    void uploadCsv(client, data.file as unknown as File);
  } catch (_e: any) {
    const e = _e as Error;

    console.warn("failed to upload csv", e);

    return ev.fail(400, { message: e.message });
  }
});

export default component$(() => {
  const uploadAction = useUploadAction();

  return (
    <>
      <h1 class="text-xl">Upload messages</h1>

      {uploadAction.isRunning ? <p>Loading...</p> : <></>}

      {uploadAction.status != null && !uploadAction.isRunning ? (
        <p>
          Status: {uploadAction.status} {uploadAction.value?.message}
        </p>
      ) : (
        <></>
      )}

      <Form class="max-w-sm p-2" action={uploadAction}>
        <div class="py-2">
          <DropBox />
        </div>
        <button
          type="submit"
          class="mb-2 me-2 rounded-full bg-blue-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Submit
        </button>
      </Form>
    </>
  );
});

const DropBox = component$(() => {
  return (
    <div class="w-fit rounded bg-gray-200 p-2">
      <label class="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
        Choose CSV file
      </label>
      <input
        class="mb-5 block w-full cursor-pointer rounded-lg border border-gray-300 bg-gray-50 text-xs text-gray-900 focus:outline-none dark:border-gray-600 dark:bg-gray-700 dark:text-gray-400 dark:placeholder-gray-400"
        name="file"
        type="file"
        accept=".csv"
      />

      <p
        class="mt-1 text-sm text-gray-500 dark:text-gray-300"
        id="file_input_help"
      >
        CSV file (max 100 MB)
      </p>
    </div>
  );
});
