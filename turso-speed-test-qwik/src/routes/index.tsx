import { component$, useSignal } from "@builder.io/qwik";
import type { RequestEventBase} from "@builder.io/qwik-city";
import { routeAction$, routeLoader$ } from "@builder.io/qwik-city";
import { tursoClient } from "~/utils/turso";
import { createReadStream } from "fs"
import { parse as parseCSV } from 'csv-parse';
import _ from 'lodash'
const {chunk} = _;

export default component$(() => {
  const data = useRouteLoader();
  const runAction = useRunAction();
  const queryAction = useQueryAction();

  const queryInput = useSignal("");
  const queryOutput = useSignal("");

  return (
    <>
      <h1>Hi 👋</h1>
      <div>
        Can't wait to see what you build with qwik!
        <br />
        Happy coding.
      </div>
      <div>
        <div>
          <span>Data</span> <br />
          <button onClick$={() => runAction.submit()}>Action</button> <br />
        </div>
        <div style={{paddingTop: '3px', paddingBottom: '3px'}}>
          <input type="text" bind:value={queryInput} />
          <button onClick$={async () => {
            const ret = await queryAction.submit({sql: queryInput.value})
            queryOutput.value = ret.value.data || ret.value.error;
          }}>Execute</button>
          <pre style={{ border: 'thin solid black' }}>{queryOutput}</pre>
        </div>
        <pre>
          COUNT: {data.value.rowCount} <br />
          {JSON.stringify(data.value.rows, null, 2)}
        </pre>
      </div>
    </>
  );
});

export const useRouteLoader = routeLoader$(async (ev) => {
  const client = tursoClient(ev);


  const ret = await client.execute("SELECT * FROM message LIMIT 10;");
  const rowCount = await client.execute("SELECT COUNT(*) FROM message;");

  return {
    rows: ret.rows,
    rowCount: rowCount.rows[0][0] as number
  };
});

export const useQueryAction = routeAction$(async (data, ev) => {
  const client = tursoClient(ev);

  try {
    const ret = await client.execute(data.sql as string);

    return {
      data: JSON.stringify(ret, null, 2)
    }
  } catch (e) {
    return {
      error: `${e}`
    }
  }
})

export const useRunAction = routeAction$(async (_, ev) => {
  console.info("Action RUN");

  doAction(ev); // RUN IN BACKGROUND
});

async function doAction(ev: RequestEventBase) {
  const client = tursoClient(ev);

  const csvFile = await createReadStream("/home/daniel/Downloads/tmp/all.csv");
  try {
    const parser = parseCSV({ delimiter: ',' })
    const parsed = csvFile.pipe(parser);

    
    const chunked = chunk((await parsed.toArray()), 250);
    
    let i = 0;
    for await (const chunk of chunked) {
      console.info(`STATUS =>>>>>>>>>>>> ${i} of ${chunked.length}`)

      const trans = await client.transaction("write");
      try {

        for (const row of chunk) {
          const [id, did, msg, ts] = row;
          trans.execute({sql: 'INSERT INTO message(id, did, content, ts) values(?, ?, ?, ?)', args: [id, did, msg, ts] })
          // client.
        }

        trans.commit();
      } catch (e) {
        console.error("Error in transaction", e);
        trans.rollback();
      }

      await (new Promise((res) => setTimeout(res, 100)));


      i += 1;
    }

  } finally {
    csvFile.close();
  }

}
