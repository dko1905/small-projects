import type { Client } from "@libsql/client";
import {parse as _parseCSV, type Options as CSVOptions, type Callback as CSVCallback} from 'csv-parse'
import { promisify } from "util";
import _ from 'lodash';

const parseCSV = promisify((a: string, b: CSVOptions, c: CSVCallback) => _parseCSV(a, b, c));

const CHUNK_SIZE = 50;

type CSVRow = [string, string, string | null, string | null];

export async function uploadCsv(client: Client, file: File) {
  const body = await file.text();
  if (body.length == 0) {
    throw Error("invalid CSV file, file is empty");
  }

  const rows = await parseCSV(body, { delimiter: ',' }) as CSVRow[];
  const rowsChunked = _.chunk(rows, CHUNK_SIZE);

  let chunkCount = 0;
  for (const chunk of rowsChunked) {
    const trans = await client.transaction("write");
    try {
      for (const row of chunk) {
        try {
          await trans.execute({sql: 'INSERT OR IGNORE INTO message2(id, did, content, ts) VALUES(?, ?, ?, ?);', args: row});
        } catch (e) {
          await sleep(150);
          throw e;
        }
      }

      await trans.commit();
      await sleep(150);
    } catch (e) {
      console.warn("error during uploading csv", e);
      await trans.rollback();
    }

    chunkCount += 1;
    console.info(`finished processing chunk ${chunkCount} of ${rowsChunked.length}`)
  }
}

function sleep(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}
