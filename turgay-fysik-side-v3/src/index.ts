const fpsCounter = document.getElementById('fpsCounter')!;
const cubeCounter = document.getElementById('cubeCounter')!;
const canvasElm = document.getElementById('canvas')! as HTMLCanvasElement;
const ctx = canvasElm.getContext("2d")!;
const autoClickElm = document.getElementById('autoclick')! as HTMLInputElement;
const autoClickRateElm = document.getElementById('autoclickrate')! as HTMLInputElement;
const widthELm = document.getElementById('width')! as HTMLInputElement;
const heightELm = document.getElementById('height')! as HTMLInputElement;
const FPSTargetELm = document.getElementById('fpsTarget')! as HTMLInputElement;
const startElm = document.getElementById('startBtn')! as HTMLButtonElement;
const stopElm = document.getElementById('stopBtn')! as HTMLButtonElement;

function loadSetting<T>(name: string): T | null {
  const itm = localStorage.getItem(name);
  try {
    if (itm == null) return null;
    else return JSON.parse(itm);
  } catch (e) {
    console.error(e);
    localStorage.removeItem(name);
    return null;
  }
}

function loadSettingOr<T>(name: string, val: T): T {
  const ret = loadSetting<T>(name)
  if (ret == null) {
    localStorage.setItem(name, JSON.stringify(val));
    return val;
  }
  return ret;
}

function saveSetting<T>(name: string, val: T) {
  localStorage.setItem(name, JSON.stringify(val));
}

function daysBetween(startDate: Date, endDate: Date) {
  // The number of milliseconds in all UTC days (no DST)
  const oneDay = 1000 * 60 * 60 * 24;

  // A day in UTC always lasts 24 hours (unlike in other time formats)
  const start = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
  const end = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

  // so it's safe to divide by 24 hours
  return (start - end) / oneDay;
}

function hsvToRgb(h: number, s: number, v: number): {r: number, g: number, b: number} {
  // Ensure h is in the range [0, 360), s and v are in [0, 1]
  h = (h % 360 + 360) % 360;
  s = Math.max(0, Math.min(1, s));
  v = Math.max(0, Math.min(1, v));

  // Calculate chroma, intermediate values, and final RGB values
  const c = v * s;
  const x = c * (1 - Math.abs((h / 60) % 2 - 1));
  const m = v - c;
  
  let r, g, b;
  if (h >= 0 && h < 60) {
    r = c; g = x; b = 0;
  } else if (h >= 60 && h < 120) {
    r = x; g = c; b = 0;
  } else if (h >= 120 && h < 180) {
    r = 0; g = c; b = x;
  } else if (h >= 180 && h < 240) {
    r = 0; g = x; b = c;
  } else if (h >= 240 && h < 300) {
    r = x; g = 0; b = c;
  } else {
    r = c; g = 0; b = x;
  }

  return {
    r: Math.round((r + m) * 255),
    g: Math.round((g + m) * 255),
    b: Math.round((b + m) * 255)
  };
}

const DEFAULT_WIDTH = loadSettingOr('gwidth', 1300);
const DEFAULT_HEIGHT = loadSettingOr('gheight', 500);
const DEFAULT_FPS_TARGET = loadSettingOr('gfps', 60);
const DEFAULT_FPS_AVERAGE_LENGTH = Math.max(Math.round(DEFAULT_FPS_TARGET / 10), 1);
const DEFAULT_AUTO_CLICK_RATE = loadSettingOr('gautoclickrate', 1);
const CUBE_SIZE = 20;
const GRAVITY = 900; 
const INITIAL_POWER = 450;
const ENERGY_LOSS = 0.005;
const MOUSE_MARGIN = 10; // Mouse margin

type Vec2 = {
  x: number,
  y: number,
};

// Default HTML value
widthELm.value = '' + DEFAULT_WIDTH;
heightELm.value = '' + DEFAULT_HEIGHT;
canvasElm.width = DEFAULT_WIDTH;
canvasElm.height = DEFAULT_HEIGHT;
FPSTargetELm.value = '' + DEFAULT_FPS_TARGET;
autoClickRateElm.value = '' + DEFAULT_AUTO_CLICK_RATE;
stopElm.disabled = true;

let runningIntervalHandle: number | null = null;

function start() {
  startElm.disabled = true;
  stopElm.disabled = false;
  const autoClick: boolean = autoClickElm.checked;
  const autoClickRate: number = Number.parseInt(autoClickRateElm.value);
  saveSetting('gautoclickrate', autoClickRate)
  const width = Number.parseInt(widthELm.value);
  const height = Number.parseInt(heightELm.value);
  saveSetting('gwidth', width);
  saveSetting('gheight', height);
  canvasElm.width = width;
  canvasElm.height = height;
  const fpsTarget = Number.parseInt(FPSTargetELm.value);
  saveSetting('gfps', fpsTarget);
  const canvasX = canvasElm.getBoundingClientRect().x;
  const canvasY = canvasElm.getBoundingClientRect().y;
  const fpsAverageLength = DEFAULT_FPS_AVERAGE_LENGTH;

  let mouseDown = false;
  let mousePos: Vec2 = {x: 0, y: 0};
  let mousePosPrev: Vec2 = {x: 0, y: 0};
  canvasElm.onmousedown = () => {
    if (autoClick) return;
    mouseDown = true;
  }
  canvasElm.onmouseup = () => {
    if (autoClick) return;
    mouseDown = false
  };
  canvasElm.onmousemove = (e: MouseEvent) => {
    if (e.x < canvasX + MOUSE_MARGIN || e.x > canvasX - MOUSE_MARGIN + width ||
        e.y < canvasY + MOUSE_MARGIN || e.y > canvasY - MOUSE_MARGIN + height) {
      if (autoClick) mouseDown = false;
    } else {
      if (autoClick) mouseDown = true;
    }
    mousePos.x = e.x - canvasX;
    mousePos.y = e.y - canvasY;
  };

  // Invert y-axies in canvas
  function toLocal(cord: Vec2): Vec2 {
    return {x: cord.x, y: height - cord.y};
  }
  function toGlobal(cord: Vec2): Vec2 {
    return {x: cord.x, y: height - cord.y};
  }

  let frameTimePreviousArr: Array<number> = [];
  let frameTimePrevious = Date.now();
  let deltaTime = 0;
  let cubes: Array<Vec2> = [];
  let cubesDelta: Array<Vec2> = [];
  function draw() {
    // Clear
    ctx.clearRect(0, 0, width, height);

    // Draw cubes
    for (let c: Vec2, i = 0; i < cubes.length; i += 1) {
      c = toGlobal(cubes[i]);
      ctx.beginPath();
      const col = hsvToRgb((cubesDelta[i].x + cubesDelta[i].y) / 800 * 255, 1, 1);
      ctx.fillStyle = `rgb(${col.r}, ${col.g}, ${col.b})`;
      ctx.fillRect(c.x, c.y - CUBE_SIZE, CUBE_SIZE, CUBE_SIZE);
      ctx.closePath();
      ctx.stroke();
    }

    // Calculate position
    for (let i = 0; i < cubes.length; i += 1) {
      const nX = cubes[i].x + cubesDelta[i].x * deltaTime;
      const nY = cubes[i].y + cubesDelta[i].y * deltaTime;

      // Check collissions
      if (nX < 0) {
        cubes[i].x = 0;
        cubesDelta[i].x *= -1;
      } else if (nY < 0) {
        cubes[i].y = 0;
        cubesDelta[i].y *= -1;
      } else if (nX > width) {
        cubes[i].x = width;
        cubesDelta[i].x *= -1;
      } else if (nY > height) {
        cubes[i].y = height;
        cubesDelta[i].y *= -1;
      } else {
        cubesDelta[i].y -= GRAVITY * deltaTime;
        // Set next position
        cubes[i].x += cubesDelta[i].x * deltaTime; 
        cubes[i].y += cubesDelta[i].y * deltaTime; 
      }
    }
    

    // Spawning
    if (mouseDown && (mousePos.x != mousePosPrev.x || mousePos.y != mousePosPrev.y || autoClick)) {
      for (let i = 0; i < autoClickRate; i += 1) {
        cubes.push(toLocal(mousePos));
        cubesDelta.push({
          x: (Math.random() * INITIAL_POWER * (Math.random() > 0.5 ? 1 : -1)) / 1.5 + (1/1.5)/2,
          y: INITIAL_POWER / 1.5 + (1/1.5)/2
        });
      }
    }

    // Game logic info
    mousePosPrev = {...mousePos};

    // Game info
    cubeCounter.innerText = `${cubes.length}`;
    const time = Date.now(); // IN MS
    deltaTime = (time - frameTimePrevious) / 1000;
    frameTimePreviousArr.push(deltaTime);
    if (frameTimePreviousArr.length > fpsAverageLength) {
      frameTimePreviousArr.shift();
    }
    const deltaTimeAvg = frameTimePreviousArr.reduce((a,c) => a + c, 0 ) / frameTimePreviousArr.length;
    const fps: number = (1 / deltaTimeAvg);
    const fpsStr: string = fps.toFixed(2).padStart(6, '0');
    const fpsTargetStr: string = (fps / fpsTarget * 100).toFixed(2).padStart(6, '0');
    fpsCounter.innerText = `${fpsStr} fps (${fpsTargetStr}% target)`;
    frameTimePrevious = time;
  }

  runningIntervalHandle = setInterval(draw, (1/fpsTarget) * 1000);
}

function stop() {
  startElm.disabled = false;
  stopElm.disabled = true;
  if (runningIntervalHandle != null) {
    clearInterval(runningIntervalHandle);
  }
}

startElm.onclick = start;
stopElm.onclick = stop;

setTimeout(() => {
  const modal = Array.from(document.querySelectorAll('.modal-box'));
  modal.forEach(e => {
    e.classList.add('hidden');
  });
}, 0);
