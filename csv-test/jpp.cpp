#include <fstream>
#include <iostream>
#include <string>

int main(void)
{
        std::ios_base::sync_with_stdio(false);
        std::ofstream datasheet("datasheet.txt", std::ofstream::out);
        if (datasheet.is_open()) {
                std::cout << "Datasheet is open!" << std::endl;
                std::string name, address, phone;
                while (std::cin >> name >> address >> phone && datasheet << name << "," << address << "," << phone << '\n');
        }
        return 0;
}
