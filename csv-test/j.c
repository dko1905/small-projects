#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <assert.h>

#define DEFAULT_SIZE 64

size_t read_column(FILE *volatile fin, char **s, size_t *volatile len, char delimiter);

int main(void)
{
	size_t n = 0, name_len = 0, address_len = 0, phone_len = 0;
	size_t name_ret = 0, address_ret = 0, phone_ret = 0;
	char *name = NULL, *address = NULL, *phone = NULL;
	const char *fout_path = "dataset.txt";
	FILE *fin = NULL, *fout = NULL;
	int status = EXIT_SUCCESS;

	/* Open files. */
	if ((fin = stdin) == NULL) {
		perror("Failed to open stdin");
		status = EXIT_FAILURE;
		goto fopen_err;
	}
	if ((fout = fopen(fout_path, "w")) == NULL) {
		perror("Failed to open fout");
		fclose(fin);
		status = EXIT_FAILURE;
		goto fopen_err;
	}

	/* Initialize strings, could also be moved into read_column. */
	name = calloc(DEFAULT_SIZE, 1);
	name_len = DEFAULT_SIZE;
	address = calloc(DEFAULT_SIZE, 1);
	address_len = DEFAULT_SIZE;
	phone = calloc(DEFAULT_SIZE, 1);
	phone_len = DEFAULT_SIZE;
	if (name == NULL || address == NULL || phone == NULL) {
		perror("Failed to allocate one or more of the strings");
		status = EXIT_FAILURE;
		goto str_err;
	}

	/* Read 3 columns, then print the columns, inefficiently. */
	while (1) {
		name_ret = read_column(fin, &name, &name_len, ',');
		if (name_ret == 0) {
			if (feof(fin)) break;
			perror("Failed to read name column");
			status = EXIT_FAILURE;
			goto io_err;
		}

		address_ret = read_column(fin, &address, &address_len, ',');
		if (address_ret == 0) {
			if (feof(fin)) break;
			perror("Failed to read address column");
			status = EXIT_FAILURE;
			goto io_err;
		}

		phone_ret = read_column(fin, &phone, &phone_len, ',');
		if (phone_ret == 0) {
			if (feof(fin)) break;
			perror("Failed to read phone column");
			status = EXIT_FAILURE;
			goto io_err;
		}

		fwrite(name, address_ret, 1, fout);
		fputc(',', fout);
		fwrite(address, address_ret, 1, fout);
		fputc(',', fout);
		fwrite(phone, phone_ret, 1, fout);
		fputc('\n', fout);
		// fprintf(fout, "%.*s,%s,%s\n", 0UL, name, address, phone);
	}

io_err:
str_err:
	if (name != NULL) free(name);
	if (address != NULL) free(address);
	if (phone != NULL) free(phone);
	fclose(fin);
	fclose(fout);
fopen_err:

	return status;
}

size_t read_column(FILE *volatile fin, char **s, size_t *volatile s_len, char delimiter)
{
	size_t free = 0;
	int c = 0;

	/* Not required, but may be a good idea? */
	// assert(fin != NULL);
	// assert(s != NULL);
	// assert(*s_len > 1); /* Must be able to hold null terminator. */

	/* Read per char into buffer `s`, if buffer is too small, reallocate. */
	while ((c = fgetc(fin)) != EOF && c != '\n' && c != delimiter) {
		/* If free + 1 is end, reallocate. */
		if (*s_len <= free + 1) {
			*s_len *= 2;
			*s = realloc(*s, *s_len);
			if (*s == NULL) {
				/* Better error handling exists.
				 * E.g. `s` is not freed. */
				perror("FUCK!!!");
				exit(1);
			}
		}

		(*s)[free++] = (char)c;
	}

	if (free > 0)
		(*s)[free++] = '\0';
	else
		(*s)[free] = '\0';

	return free;
}
