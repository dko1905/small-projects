import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Blackjack</h1>
    <a routerLink="/play">Play</a>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
}
