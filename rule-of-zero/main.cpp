#include <string>

class Cat {
public:
	Cat(const std::string &name, const int &age)
	: _name{name}
	, _age{age} {};

private:
	const std::string &_name;
	const int _age;
};

int main () {
	const char *a = "abc";

	Cat cat{a, 2};

	Cat cat2 = cat;

	return 0;
}
