import { OakApp } from "./src/oak.ts";
import { ExpressApp } from "./src/express.ts";
import { DenoApp } from "./src/deno-native.ts";

export class Program {
  constructor() {}

  async main(): Promise<void> {
    const mode = Deno.args.at(0);
    const port = 3000;

    switch (mode) {
      case "oak":
        return await new OakApp().main(port);
      case "express":
        return await new ExpressApp().main(port);
      case "deno":
        return await new DenoApp().main(port);

      default:
        throw new Error("No mode provided");
    }
  }
}

if (import.meta.main) {
  try {
    new Program().main();
  } catch (e) {
    console.error(e);
  }
}
