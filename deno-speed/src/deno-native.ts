import { IApp } from "./IApp.ts";

export class DenoApp implements IApp {
  async main(port: number): Promise<void> {
    await Deno.serve({
      hostname: "127.0.0.1",
      port,
    }, (req) => {
      if (this.ROOT_MATCH.exec(req.url)) {
        return this.root(req);
      }

      const helloMatch = this.HELLO_MATCH.exec(req.url);
      if (helloMatch) {
        return this.hello(req, helloMatch.pathname.groups.name || null);
      }

      return new Response("Not Found", { status: 404 });
    });
  }

  private ROOT_MATCH = new URLPattern({ pathname: "/" });
  private root(_req: Request) {
    return new Response("Hello World!");
  }

  private HELLO_MATCH = new URLPattern({ pathname: "/hello/:name" });
  private hello(_req: Request, name: string | null) {
    const body = JSON.stringify({ message: `Hello ${name}!` });

    const headers = new Headers();
    headers.set("Content-Type", "json");
    return new Response(body, { status: 418, headers });
  }
}
