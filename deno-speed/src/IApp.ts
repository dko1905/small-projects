export interface IApp {
  main(port: number): Promise<void>;
}
