import { IApp } from "./IApp.ts";

// @deno-types="npm:@types/express@5.0.0"
import express from "npm:express@5.0.1";
import { Request, Response } from "npm:express@5.0.1";

// @deno-types="npm:@types/response-time@2.3.8"
import responseTime from "npm:response-time@2.3.3";

export class ExpressApp implements IApp {
  app!: express.Express;

  async main(port: number): Promise<void> {
    this.app = express();

    this.app.disable("x-powered-by");
    this.app.disable("etag");

    this.app.use(responseTime()); // Doesn't work on Deno
    this.app.get("/", this.root);
    this.app.get("/hello/:name", this.hello);

    await this.app.listen(port, () => {
      console.info("Express.js listening on port", port);
    });
  }

  private root(_req: Request, res: Response) {
    res.send("Hello World!");
  }

  private hello(req: Request, res: Response) {
    const name = req.params?.name;

    res.status(418).json({ message: `Hello ${name}!` });
  }
}
