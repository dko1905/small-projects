import { IApp } from "./IApp.ts";

import { Application, Context, Next, Router, Status } from "jsr:@oak/oak@14";

export class OakApp implements IApp {
  app!: Application;
  router!: Router;

  async main(port: number): Promise<void> {
    this.app = new Application();
    this.router = new Router();

    this.router.get("/", this.root);
    this.router.get("/hello/:name", this.hello);

    this.app.use(this.timing);
    this.app.use(this.router.routes());
    this.app.use(this.router.allowedMethods());

    console.info("Oak listening on port", port);
    await this.app.listen({ port });
  }

  private root(ctx: Context) {
    ctx.response.body = "Hello World";
  }

  private hello(
    ctx: Context & { params?: Record<string, string | undefined> },
  ) {
    const name = ctx?.params?.name;

    ctx.response.body = JSON.stringify({ message: `Hello ${name}!` });
    ctx.response.type = "json";
    ctx.response.status = Status.Teapot;
  }

  private async timing(ctx: Context, next: Next) {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.response.headers.set("X-Response-Time", `${ms}ms`);
  }
}
