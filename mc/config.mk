# Flags
MYCPPFLAGS = # My Preprocessor
MYCFLAGS = -std=c99 -Wall -Wextra -pedantic -fno-strict-aliasing \
           $(INCS) $(MYCPPFLAGS) $(CPPFLAGS) $(CFLAGS) # My C-flags
MYLDFLAGS = $(LIBS) $(LDFLAGS) # My LD flags
