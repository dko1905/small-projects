#include "cat.h"
#include <stdio.h>

typedef test_animals_Cat Cat;

static int test_animals_Cat_meow(Cat *self) {
	printf("Meow: %i\n", self->msg);
	return self->msg;
}

Cat test_animals_Cat_init(int msg) {
	return (Cat){
		.msg = msg,

		.meow = &test_animals_Cat_meow,
		.valid = true
	};
}

void test_animals_Cat_free(Cat *cat) {
	cat->valid = false;
	cat->msg = 0;
}
