
#define fn1(_fn1_ret, _fn1_name, _fn1_class1_ns, _fn1_class1_name) \
	_fn1_ret (*_fn1_name)(struct _fn1_class1_ns##_##_fn1_class1_name);

#define class1(_class1_ns, _class1_name, _class1_decs, ...) \
	struct _class1_ns##_##_class1_name { \
		_class1_decs \
		fn1(__VA_ARGS__, _class1_ns, _class1_name) \
	};


class1(test_animals, Dog,
	const char *msg;,
	int, meow
)

#define class(_ns, _def) \
	struct _ns##_##_def;

#define fn(_ns, _ret, _name) \
	_ret (*_name)(struct _ns *self);

class(test_animals, Cat {
	const char *msg;

	fn(test_animals_Cat, int, meow)
});


