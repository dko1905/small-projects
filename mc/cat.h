#ifndef CAT_H
#define CAT_H
#include <stdbool.h>

// Combine struct name and namespace
typedef struct test_animals_Cat {
	int msg;

	int (*meow)(struct test_animals_Cat *self);

	bool valid;
} test_animals_Cat;

// Constructor and destructor declarations.
test_animals_Cat test_animals_Cat_init(int msg);
void test_animals_Cat_free(test_animals_Cat *cat);

#endif /* CAT_H */
