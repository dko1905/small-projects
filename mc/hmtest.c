#include <stdio.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

int main() {
	struct {wchar_t *key; wchar_t *value;} *hm = NULL;

	hmput(hm, L"123", L"abc");
	hmput(hm, L"1234", L"dabc");
	hmput(hm, L"1235", L"1abc");

	wchar_t *str = malloc(4 * sizeof(wchar_t));
	memcpy(str, L"123", 4 * sizeof(wchar_t));

	printf("get: %ls\n", hmget(hm, str));

	for (int i = 0; i < hmlen(hm); ++i) {
		printf("%i: %ls,%ls\n", i, hm[i].key, hm[i].value);
	}

	hmfree(hm);
	free(str);
}
