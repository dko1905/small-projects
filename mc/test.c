


#define class(_ns, _def) \
	struct _ns##_##_def;
#define fn(_ns, _ret, _name) \
	_ret (*_name)(struct _ns *self);

class(test_animals, Cat {
	const char *msg;

	fn(test_animals_Cat, int, meow)
});
