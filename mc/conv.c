#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <wchar.h>
#include <string.h>
#include <locale.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

/* Globals */
static wchar_t *block_buffer = NULL;
static size_t block_free = 0;
static size_t block_size = sizeof(wchar_t); /* Allocate space for one char. */

/* Read the next block of text. (only a-z, A-Z, 0-9 and _) */
void die(const char *msg, int exit_code);
wchar_t *get_next_block(FILE *stream);
size_t calc_hash(wchar_t *s, size_t len);

void do_thing(FILE *input, FILE *output);
/* Printing */
static bool p_debug_on = true; /* Print debug. */
#define p_debug(...) do { \
	if (p_debug_on == true) { \
		fprintf(stderr, __VA_ARGS__); \
	} \
} while(false);


int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "");

	FILE *input = stdin;
	FILE *output = stdout;
	do_thing(input, output);

	if (block_buffer != NULL) free(block_buffer);
	return EXIT_SUCCESS;
}

void die(const char *msg, int exit_code) {
	perror(msg);
	exit(exit_code);
}

void do_thing(FILE *input, FILE *output) {
	wchar_t c = 0;
	/* states:
	 * 0 - normal
	 * 1 - start comment
	 * 2 - c comment
	 * 3 - cpp comment
	 * 4 - stop comment (c only)
	 * 5 - skip char (only comments) (c & cpp)
	 * 10 - start command
	 * 11 - command stop (just like skip char)
	 * 20 - string skip
	 * 21 - string " start
	 * 22 - string ' start
	 */
	unsigned int state = 0;
	/* `command` stores the current found command, `command` is set
	 * to `""` when no command is active.
	 */
	wchar_t *command = L"";
	/* Hashmap to store all the imports. hm does not support wide char,
	 * therfore we will need to conver to utf8 and use sh. */
	struct {size_t key; wchar_t *value;} *import_map = NULL;

	/* Main loop */
	while ((c = fgetwc(input)) != WEOF) {
		/* Handle comments */
		if (state == 5 || state == 11 || state == 20) {
			state = 0;
		}
		if (state == 0) {
			if (c == L'/') {
				state = 1;
			} else if (c == L'@') {
				state = 10;
			}
		} else if (state == 1) {
			if (c == L'*') {
				state = 2;
			} else if (c == L'/') {
				state = 3;
			} else {
				state = 0;
			}
		} else if (state == 2) {
			if (c == L'*') {
				state = 4;
			}
		} else if (state == 3) {
			if (c == L'\n') {
				state = 0;
			}
		} else if (state == 4) {
			if (c == L'/') {
				state = 5;
			} else {
				state = 2;
			}
		}
		/* Handle strings/quotes */
		if (state == 0) {
			if (c == '"') {
				state = 21;
			} else if (c == '\'') {
				state = 22;
			}
		} else if (state == 21) {
			if (c == '"') {
				state = 20;
			}
		} else if (state == 23) {
			if (c == '\'') {
				state = 20;
			}
		}
		/* Handle commands */
		if (state == 10) {
			command = get_next_block(input);
			p_debug("command: %ls\n", command);
			if (command == NULL) {
				p_debug("command WEOF\n");
				return;
			}

			if (wcscmp(command, L"import") == 0) {
				wchar_t *tmp = NULL, *fullname = NULL, *name = NULL;

				p_debug("import command detected\n");

				/* Read next block into fullname. */
				tmp = get_next_block(input);
				if (tmp == NULL) die("Failed to get next block", 1);
				fullname = malloc((wcslen(tmp) + 1) * sizeof(wchar_t));
				if (fullname == NULL) die("Failed to allocate space for fullname", 2);
				memcpy(fullname, tmp, (wcslen(tmp) + 1) * sizeof(wchar_t));
				/* Read next next block into name. */
				tmp = get_next_block(input); /* skip this */
				if (tmp == NULL) die("Failed to get next block", 1);
				tmp = get_next_block(input);
				if (tmp == NULL) die("Failed to get next block", 1);
				name = malloc((wcslen(tmp) + 1) * sizeof(wchar_t));
				if (name == NULL) die("Failed to allocate space for name", 2);
				memcpy(name, tmp, (wcslen(tmp) + 1) * sizeof(wchar_t));
				/* Print debug. */
				p_debug("import:\n\tfullname: \"%ls\"\n\tname: \"%ls\"\n", fullname, name);
				/* Replace dots with underscores. */
				for (size_t n = 0; n < wcslen(fullname); ++n) {
					if (fullname[n] == L'_';
					}
				}
				/* Finally print typedef. */
				fprintf(output, "typedef %ls %ls;\n", fullname, name);
				hmput(import_map, calc_hash(name, wcslen(name)), fullname);
				// We need to keep it in the hashmap.
				// free(fullname);
				// free(name);
			} else if (wcscmp(command, L"new") == 0) {
				wchar_t *typename = NULL, *fullname = NULL;
				size_t typename_len = 0;

				p_debug("new command detected\n");

				/* Read next block into typename. */
				typename = get_next_block(input);
				if (typename == NULL) die("Failed to get next block", 1);
				/* Read from hashmap. */
				fullname = hmgets(import_map, calc_hash(typename, wcslen(typename))).value;
				/* Print debug. */
				p_debug("new:\n\ttypename: \"%ls\"\n\tfullname: \"%ls\"\n",
				        typename, fullname);

				fprintf(output, "%ls_init", fullname);

				free(typename);
			} else if (wcscmp(command, L"free") == 0) {


				p_debug("free command detected\n");
			}

			command = L"";
			state = 11;
		}

		if (state >= 0 && state <= 5) {
			printf("%lc", c);
		}
	}

	/* Free the import map. */
	for (size_t i = 0; i < hmlen(import_map); ++i) {
		free(import_map[i].value);
	}
	hmfree(import_map);
}

wchar_t *get_next_block(FILE *stream) {
	wchar_t c = 0;

	block_free = 0;

	if (block_buffer == NULL)
		block_buffer = malloc(block_size);
	if (block_buffer == NULL)
		die("Failed to allocate memory", 2);

	/* Skip spaces. */
	while (
		(c = fgetwc(stream)) != WEOF &&
		(c == ' ' || c == '\t')
	) {}
	if (c == WEOF) return NULL;
	/* Read next block of "text". */
	while (
		c != WEOF &&
		((c >= '0' && c <= '9') ||
		(c >= 'A' && c <= 'Z') ||
		(c >= 'a' && c <= 'z') ||
		(c == '.' || c == '_')) ||
		(c > 127)
	) {
		block_buffer[block_free++] = c;

		if (block_free >= (block_size / 4) - 1) {
			block_size *= 2;
			block_buffer = realloc(block_buffer, block_size);
			if (block_buffer == NULL)
				die("Failed to allocate memory", 2);
		}

		c = fgetwc(stream);
	}
	if (c == WEOF) return NULL; /* This might break things. */
	else ungetwc(c, stream);

	block_buffer[block_free] = L'\0';

	return block_buffer;
}

size_t calc_hash(wchar_t *s, size_t len) {
	size_t hash = 0;
	for (size_t i = 0; i < len; ++i) {
		hash = (31 * hash + s[i]);
	}

	return hash;
}
