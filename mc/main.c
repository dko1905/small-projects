#include "cat.h"

typedef test_animals_Cat Cat;

int main(int argc, char *argv[]){
	(void)argc; (void)argv;

	Cat cat = test_animals_Cat_init(123);
	if (cat.valid == false) {
		return 1;
	}

	cat.meow(&cat);

	test_animals_Cat_free(&cat);

	return 0;
}
