#[derive(Debug)]
struct Board {
    nboard: [u8; 6],
    sboard: [u8; 6],
    npoint: u8,
    spoint: u8,
    last_player: Player,
}

#[derive(Debug)]
enum Player {
    North,
    South,
}

impl Board {
    fn new() -> Board {
        Self {
            nboard: [4, 4, 4, 4, 4, 4],
            sboard: [4, 4, 4, 4, 4, 4],
            npoint: 0,
            spoint: 0,
            last_player: Player::South,
        }
    }
    fn moves(&self) -> Vec<BMove> {
        let mut moves = Vec::new();
        match self.last_player {
            Player::North => {
                for (i, h) in self.sboard.iter().enumerate() {
                    if *h > 0 {
                        moves.push(BMove::Pick(Player::South, i.try_into().unwrap()))
                    }
                }
            }
            Player::South => {
                for (i, h) in self.nboard.iter().enumerate() {
                    if *h > 0 {
                        moves.push(BMove::Pick(Player::North, i.try_into().unwrap()))
                    }
                }
            }
        };
        moves
    }
}

#[derive(Debug)]
enum BMove {
    Pick(Player, u8),
}

fn main() {
    let mut board = Board::new();
    board.last_player = Player::North;
    let moves = board.moves();
    println!("Moves: {:?}", &moves);
    println!("Board: {:?}", &board)
}
