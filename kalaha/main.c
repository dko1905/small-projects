#define _POSIX_C_SOURCE 200112L // POSIX.1-2001

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <assert.h>

#ifndef VERSION
#define VERSION "Something went wrong with VERSION"
#endif


#define MAX_BOARD_SIZE (3*2)

struct Board {
	unsigned int size;
	/* Special places: size / 2 - 1 & size - 1 */
	unsigned int sa, sb;
	unsigned short cells[MAX_BOARD_SIZE];
};

struct KalahaTree {
	bool finished;
	char winner;
};

/**
 * @brief Do one move on board.
 *
 * @param player Which players turn is it.
 * @param position Which position to move balls from (must have more than 0 balls.)
 * @param board Board to move balls on.
 */
int do_move(char *player, unsigned int *position, struct Board *board);
void do_tree(char player, int ret, struct Board board);
void do_tree2(char player, unsigned int pos, int ret, struct Board board);

char move(char player, int pos, int *last, struct Board *board);
void test(char player, struct KalahaTree *ktree, struct Board board);
void print_board(const struct Board *board);

int main(int argc, char *argv[])
{
	(void)argc; (void)argv;

	printf("# Version: "VERSION"\n");
	printf("digraph G {\n");
	printf("  node [style=filled];\n");

	struct Board board = {0};

	board.size = MAX_BOARD_SIZE;
	for (size_t n = 0; n < MAX_BOARD_SIZE; ++n) {
		board.cells[n] = 3;
	}
	board.cells[MAX_BOARD_SIZE / 2 - 1] = 0;
	board.cells[MAX_BOARD_SIZE - 1] = 0;
	board.sa = MAX_BOARD_SIZE / 2 - 1;
	board.sb = MAX_BOARD_SIZE - 1;

	do_tree2('a', 0, 0, board);
	do_tree2('a', 1, 0, board);
	do_tree2('b', 3, 0, board);
	do_tree2('b', 4, 0, board);

	printf("}\n");

	return 0;
}

int do_move(char *player, unsigned int *position, struct Board *board)
{
	/* Balls left in hand. */
	unsigned short left = 0;
	/* Previous position. */
	unsigned int prev = 0;
	/* Special position a and b. */
	unsigned int sa = 0, sb = 0;

	sa = board->size / 2 - 1;
	sb = board->size - 1;
	left = board->cells[*position];
	board->cells[(*position)++] = 0;

	while (left > 0) {
		/* Score positions */
		if (*position == sa && *player == 'b') {
			(*position)++;
		} else if (*position == sb && *player == 'a') {
			(*position) = 0;
		}

		/* +1 to each position + 1 */
		prev = *position;
		board->cells[(*position)++]++;
		left--;

		/* End of board */
		if (*position > (board->size - 1)) {
			*position = 0;
		}
	}

	*position = prev;

	/* Score positions */
	if (*position == sa && *player == 'a') {
		/* Player 'a' plays again. */
		return 1;
	} else if (*position == sb && *player == 'b') {
		/* Player 'b' plays again. */
		return 1;
	}

	/* If end has balls. */
	if (board->cells[prev] > 1) {
		// if (*player == 'a' && prev < sa) {
		// 	return 2;
		// } else if (*player == 'b' && prev > sa && prev < sb) {
		// 	return 2;
		// }
		return 2;
	}

	/* Normal switch */
	if (*player == 'a') *player = 'b';
	else *player = 'a';

	return 0;
}

void do_tree2(char player, unsigned int pos, int ret, struct Board board)
{
	unsigned int pos_orig = pos;
	char player_orig = player;

	if (ret == 0 || ret == 1) {
		printf("\"");
		print_board(&board);
		printf("\" -> ");
	}
	ret = do_move(&player, &pos, &board);

	/* There are balls available at pos. */
	while (player == player_orig && ret == 2) {
		ret = do_move(&player, &pos, &board);
	}

	if (ret == 0 || ret == 1) {
		printf("\"");
		print_board(&board);
		printf("\"\n");
	}

	if (player == 'a') {
		for (pos = 0; pos < board.sa; pos++) {
			if (board.cells[pos] > 0) {
				do_tree2(player, pos, ret, board);
			}
		}
	} else {
		for (pos = board.sa + 1; pos < board.sb; pos++) {
			if (board.cells[pos] > 0) {
				do_tree2(player, pos, ret, board);
			}
		}
	}
}

void do_tree(char player, int ret, struct Board original)
{
	struct Board board = original;
	unsigned int pos = 0, pos_orig = 0;


	if (player == 'a') {
		for (pos_orig = 0; pos_orig < board.sa; pos_orig++) {
			pos = pos_orig;
			if (board.cells[pos] > 0) {
				ret = do_move(&player, &pos, &board);

				/* There are balls available at pos. */
				while (player == 'a' && ret == 2) {
					ret = do_move(&player, &pos,
					              &board);
				}

				/* If normal switch or play again. */
				do_tree(player, ret, board);

				memcpy(&board, &original, sizeof(board));
			}
		}
	} else {
		for (pos_orig = board.sa + 1; pos_orig < board.sb; pos_orig++) {
			pos = pos_orig;
			if (board.cells[pos] > 0) {
				ret = do_move(&player, &pos, &board);

				/* There are balls available at pos. */
				while (player == 'b' && ret == 2) {
					ret = do_move(&player, &pos,
					              &board);
				}

				/* If normal switch or play again. */
				do_tree(player, ret, board);

				memcpy(&board, &original, sizeof(board));
			}
		}
	}
}


void print_board(const struct Board *board)
{
	printf("%02i ", board->cells[board->sa]);
	for (size_t n = board->sa; n > 0; n--) {
		printf("%02i", board->cells[n - 1]);
	}
	printf(" ");
	for (size_t n = board->sa + 1; n < board->sb; n++) {
		printf("%02i", board->cells[n]);
	}
	printf(" %02i", board->cells[board->sb]);
}
