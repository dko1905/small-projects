package xyz._190405.openWPM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenWpmApplication {
	public static void main(String[] args) {
		SpringApplication.run(OpenWpmApplication.class, args);
	}
}
