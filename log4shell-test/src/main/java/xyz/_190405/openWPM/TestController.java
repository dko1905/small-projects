package xyz._190405.openWPM;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {
	private static final Logger logger = LogManager.getLogger(TestController.class);

	@PostMapping("/hello")
	public String hello(@RequestBody String body) {
		String str = "" + body;

		logger.error(str);

		return str;
	}
}
