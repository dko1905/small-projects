# Rust on ESP8266 (v2025.0.0)

Project for building for ESP8266 chip.

## Nix

```sh
# Install using espup; see below
...
# Load dev env; todo replace when merged into nixpkgs
nix develop github:dko1905/nixpkgs-esp-dev-dako#esp8266-nonos-sdk
```

## Manual Installation


Install older version of `esp` Rust compiler for compatibility with older dependencies.

```sh
espup install --name esp-1.75.0.0 --toolchain-version 1.75.0.0
# To uninstall
#espup uninstall -n esp-1.75.0.0
```

