import { WasmCalculator } from "./wasmCalculator";
import { hsv_to_rgb, rgb_to_hex } from "./helpers";
import { State } from "./state.ts";

const MAX_CUBES = 19000;

type Ctx = CanvasRenderingContext2D;

const _ccord_to_dcord = (
  width: number,
  height: number,
  x: number,
  y: number
) => {
  return [x - width / 2, (y - height / 2) * -1];
};
const _dcord_to_ccord = (state: State, x: number, y: number) => {
  return [x + state.canvas_width / 2, y * -1 + state.canvas_height / 2];
};

const draw = (ctx: Ctx, wc: WasmCalculator, state: State) => {
  const { cube_len, cubes_x, cubes_y, cubes_size, cubes_color } = state;

  let i = 0;
  while (i < cube_len) {
    // Don't draw out of bounds

    ctx.beginPath();
    const [x, y] = _dcord_to_ccord(state, cubes_x[i], cubes_y[i]);
    const r = (cubes_color[i] >> 16) & 0xFF;
    const g = (cubes_color[i] >> 8) & 0xFF;
    const b = cubes_color[i] & 0xFF;
    ctx.fillStyle = rgb_to_hex(r, g, b);
    ctx.rect(x - cubes_size[i] / 2, y - cubes_size[i] / 2, cubes_size[i], cubes_size[i]);
    ctx.fill();
    ctx.closePath();

    i += 1;
  }
};

const update = (ctx: Ctx, counter: HTMLSpanElement, wc: WasmCalculator, state: State) => {
  const { pointer } = state;

  if (pointer.moved) {
    pointer.moved = false;
    if (pointer.clicked) {
      pointer.clicked = false;
    }
    if (pointer.click_down) {
      if (state.cube_len + 1 < MAX_CUBES) {
        state.cubes_x[state.cube_len] = pointer.x;
        state.cubes_y[state.cube_len] = pointer.y;
        state.cubes_vx[state.cube_len] =
          (Math.random() > 0.5 ? -1 : 1) * Math.random() * 5;
        state.cubes_vy[state.cube_len] = 5;
        state.cubes_size[state.cube_len] = 20;
        state.cubes_color[state.cube_len] = 0xf000ff;
        state.cube_len += 1;
      }
    }
  }

  counter.innerText = state.cube_len;

  wc!.cubes_phys(state);
};

const main = async () => {
  const canvas = document.getElementById("canvas") as HTMLCanvasElement;
  const counter = document.getElementById("counter") as HTMLSpanElement;
  const ctx = canvas.getContext("2d")!;
  const state = new State();
  const wc = new WasmCalculator();
  await wc.init();
  state.width = canvas.width / 2;
  state.height = canvas.height / 2;
  state.canvas_width = canvas.width;
  state.canvas_height = canvas.height;
  state.canvas_offset_x = canvas.getBoundingClientRect().x;
  state.canvas_offset_y = canvas.getBoundingClientRect().y;
  state.cube_len = 0;
  state.cubes_x = wc.create_arr_f32(MAX_CUBES);
  state.cubes_y = wc.create_arr_f32(MAX_CUBES);
  state.cubes_vx = wc.create_arr_f32(MAX_CUBES);
  state.cubes_vy = wc.create_arr_f32(MAX_CUBES);
  state.cubes_size = wc.create_arr_f32(MAX_CUBES);
  state.cubes_color = wc.create_arr_u32(MAX_CUBES);

  console.log(wc);

  canvas.addEventListener("mousemove", (ev) => {
    state.pointer.moved = true;
    [state.pointer.x, state.pointer.y] = _ccord_to_dcord(
      state.canvas_width,
      state.canvas_height,
      ev.clientX - state.canvas_offset_x,
      ev.clientY - state.canvas_offset_y
    );
  });

  canvas.addEventListener("mousedown", (ev) => {
    state.pointer.moved = true;
    state.pointer.clicked = true;
    state.pointer.click_down = true;

    [state.pointer.x, state.pointer.y] = _ccord_to_dcord(
      state.canvas_width,
      state.canvas_height,
      ev.clientX - state.canvas_offset_x,
      ev.clientY - state.canvas_offset_y
    );
  });

  canvas.addEventListener("mouseup", (ev) => {
    state.pointer.moved = true;
    state.pointer.click_down = false;

    [state.pointer.x, state.pointer.y] = _ccord_to_dcord(
      state.canvas_width,
      state.canvas_height,
      ev.clientX - state.canvas_offset_x,
      ev.clientY - state.canvas_offset_y
    );
  });

  const loop = () => {
    ctx.clearRect(0, 0, state.canvas_width, state.canvas_height);
    update(ctx, counter, wc, state);
    draw(ctx, wc, state);
    window.requestAnimationFrame(loop);
  };
  loop();
};

window.addEventListener("load", main);
