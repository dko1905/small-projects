export class Pointer {
  x = 0;
  y = 0;
  moved = false;
  click_down = false;
  clicked = false;
}

export class State {
  g = 9.82;
  delta = 0.01;
  width = 500;
  height = 500;
  canvas_width = 500;
  canvas_height = 500;
  canvas_offset_x = 0;
  canvas_offset_y = 0;
  cube_len: number = 0;
  cubes_x: Float32Array | null = null;
  cubes_y: Float32Array | null = null;
  cubes_vx: Float32Array | null = null;
  cubes_vy: Float32Array | null = null;
  cubes_size: Float32Array | null = null;
  cubes_color: Uint32Array | null = null;
  pointer: Pointer = new Pointer();
}
