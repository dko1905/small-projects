import zigWasmUrl from "url:./zig-wasm.wasm";
import { State } from "./state";

export class WasmCalculator {
  _mem: WebAssembly.Memory | null = null;
  _mem_offset: number = 0;
  _add: any | null = null;
  _cubes_phys: any | null = null;

  async init() {
    const wa = await WebAssembly.instantiateStreaming(fetch(zigWasmUrl), {
      env: {
        print_f32: (id: number, n: number) =>
          console.log(`print_f32(${id}): ${n}`),
      },
    });
    console.log(wa);
    let { add, cubes_phys, memory } = wa.instance.exports as any;
    this._mem = memory;
    this._add = add;
    this._cubes_phys = cubes_phys;
  }

  create_arr_f32(length: number): Float32Array {
    const arr = new Float32Array(this._mem!.buffer, this._mem_offset, length);
    this._mem_offset += length * 4;
    return arr;
  }

  create_arr_u32(length: number): Uint32Array {
    const arr = new Uint32Array(this._mem!.buffer, this._mem_offset, length);
    this._mem_offset += length * 4;
    return arr;
  }

  add(arr: number[]): number {
    if (this._add == null) throw Error("WASM not loaded");
    const _arr = new Float32Array(this._mem!.buffer, 0, arr.length);
    _arr.set(arr);
    const result = this._add(_arr.byteOffset, _arr.length);
    console.log("Arr: ", _arr);
    return result;
  }

  cubes_phys(state: State): void {
    if (this._cubes_phys == null) throw Error("WASM not loaded");

    this._cubes_phys(
      state.width,
      state.height,
      state.g,
      state.delta,
      state.cube_len,
      state.cubes_x!.byteOffset,
      state.cubes_y!.byteOffset,
      state.cubes_vx!.byteOffset,
      state.cubes_vy!.byteOffset,
      state.cubes_size!.byteOffset,
      state.cubes_color!.byteOffset
    );
  }
}
