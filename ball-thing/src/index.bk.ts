import { WasmCalculator } from "./wasmCalculator";

let a = new WasmCalculator();
a.init()
console.log(a)

class GameCube {
  x = 0;
  y = 0;
  vx = 0;
  vy = 0;
  size = 0;
  weight = 1;
}

class Pointer {
  x = 0;
  y = 0;
  moved = false;
  click_down = false;
  clicked = false;
}

class State {
  g = 9.82;
  delta = 0.01;
  width = 500;
  height = 500;
  canvas_width = 500;
  canvas_height = 500;
  canvas_offset_x = 0;
  canvas_offset_y = 0;
  cubes: GameCube[] = [];
  pointer: Pointer = new Pointer();
}

type Ctx = CanvasRenderingContext2D;

const _ccord_to_dcord = (
  width: number,
  height: number,
  x: number,
  y: number
) => {
  return [x - width / 2, (y - height / 2) * -1];
};
const _dcord_to_ccord = (state: State, x: number, y: number) => {
  return [x + state.canvas_width / 2, y * -1 + state.canvas_height / 2];
};
function _HSVtoRGB(h, s, v) {
  var r, g, b, i, f, p, q, t;
  if (arguments.length === 1) {
    (s = h.s), (v = h.v), (h = h.h);
  }
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0:
      (r = v), (g = t), (b = p);
      break;
    case 1:
      (r = q), (g = v), (b = p);
      break;
    case 2:
      (r = p), (g = v), (b = t);
      break;
    case 3:
      (r = p), (g = q), (b = v);
      break;
    case 4:
      (r = t), (g = p), (b = v);
      break;
    case 5:
      (r = v), (g = p), (b = q);
      break;
  }
  return {
    r: Math.round(r * 255),
    g: Math.round(g * 255),
    b: Math.round(b * 255),
  };
}
function _componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function _rgbToHex(r, g, b) {
  return "#" + _componentToHex(r) + _componentToHex(g) + _componentToHex(b);
}

const draw = (ctx: Ctx, state: State) => {
  for (const cube of state.cubes) {
    ctx.beginPath();
    const [x, y] = _dcord_to_ccord(state, cube.x, cube.y);
    const speed = Math.sqrt(cube.vx * cube.vx + cube.vy * cube.vy);
    const energy = (1/2 * cube.weight * speed);
    const color = _HSVtoRGB(energy / 4, 1, 1);
    ctx.fillStyle = _rgbToHex(color.r, color.g, color.b);
    ctx.moveTo(x - cube.size / 2, y - cube.size / 2);
    ctx.lineTo(x + cube.size / 2, y - cube.size / 2);
    ctx.lineTo(x + cube.size / 2, y + cube.size / 2);
    ctx.lineTo(x - cube.size / 2, y + cube.size / 2);
    ctx.fill();
  }
};

const update = (ctx: Ctx, state: State) => {
  if (state.pointer.moved) {
    state.pointer.moved = false;
    if (state.pointer.clicked) {
      state.pointer.clicked = false;
    }
    if (state.pointer.click_down) {
      const cube = new GameCube();
      cube.x = state.pointer.x;
      cube.y = state.pointer.y;
      cube.size = 20;
      cube.vy = 5;
      cube.vx = (Math.random() > 0.5 ? -1 : 1) * Math.random() * 5;
      state.cubes.push(cube);
    }
  }

  for (const cube of state.cubes) {
    let slow_down = false;
    if (cube.x + cube.size / 2 > state.width) {
      cube.vx *= -1;
      slow_down = true;
    } else if (cube.x - cube.size / 2 < state.width * -1) {
      cube.vx *= -1;
      slow_down = true;
    }

    if (cube.y + cube.size / 2 > state.height) {
      cube.vy *= -1;
      slow_down = true;
    } else if (cube.y - cube.size / 2 < state.height * -1) {
      cube.vy *= -1;
      slow_down = true;
    }

    cube.x += cube.vx;
    cube.y += cube.vy;
    cube.vy -= state.g * state.delta;

    if (slow_down) {
      cube.vx *= 0.9;
      cube.vy *= 0.9;
    }
  }
};

const main = async () => {
  const canvas = document.getElementById("canvas") as HTMLCanvasElement;
  const ctx = canvas.getContext("2d")!;
  const state = new State();
  state.width = canvas.width / 2;
  state.height = canvas.height / 2;
  state.canvas_width = canvas.width;
  state.canvas_height = canvas.height;
  state.canvas_offset_x = canvas.getBoundingClientRect().x;
  state.canvas_offset_y = canvas.getBoundingClientRect().y;

  canvas.addEventListener("mousemove", (ev) => {
    state.pointer.moved = true;
    [state.pointer.x, state.pointer.y] = _ccord_to_dcord(
      state.canvas_width,
      state.canvas_height,
      ev.clientX - state.canvas_offset_x,
      ev.clientY - state.canvas_offset_y
    );
  });

  canvas.addEventListener("mousedown", (ev) => {
    state.pointer.moved = true;
    state.pointer.clicked = true;
    state.pointer.click_down = true;

    [state.pointer.x, state.pointer.y] = _ccord_to_dcord(
      state.canvas_width,
      state.canvas_height,
      ev.clientX - state.canvas_offset_x,
      ev.clientY - state.canvas_offset_y
    );
  });

  canvas.addEventListener("mouseup", (ev) => {
    state.pointer.moved = true;
    state.pointer.click_down = false;

    [state.pointer.x, state.pointer.y] = _ccord_to_dcord(
      state.canvas_width,
      state.canvas_height,
      ev.clientX - state.canvas_offset_x,
      ev.clientY - state.canvas_offset_y
    );
  });

  const loop = () => {
    ctx.clearRect(0, 0, state.canvas_width, state.canvas_height);
    update(ctx, state);
    draw(ctx, state);
    window.requestAnimationFrame(loop);
  };
  loop();
};

window.addEventListener('load', main)
