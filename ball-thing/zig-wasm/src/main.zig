const std = @import("std");
const testing = std.testing;

extern fn print_f32(id: f32, n: f32) void;

export fn add(arr: [*]f32, len: usize) f32 {
    var sum: f32 = 0;
    var i: usize = 0;
    while (i < len) : (i += 1) {
        sum += arr[i];
    }
    return sum;
}

inline fn collides_wall(x: f32, size: f32, width: f32) u8 {
    if (x + size / 2 > width) {
        return 1; // Right
    } else if (x - size / 2 < width * -1) {
        return 2; // Left
    } else {
        return 0;
    }
}

inline fn collides_ground(y: f32, size: f32, height: f32) u8 {
    if (y + size / 2 > height) {
        return 1; // Top
    } else if (y - size / 2 < height * -1) {
        return 2; // Bottom
    } else {
        return 0;
    }
}

export fn cubes_phys(
    width: f32,
    height: f32,
    g: f32,
    delta: f32,
    cube_len: i32,
    cubes_x: [*]f32,
    cubes_y: [*]f32,
    cubes_vx: [*]f32,
    cubes_vy: [*]f32,
    cubes_size: [*]f32,
    cubes_color: [*]u32,
) void {
    @setFloatMode(.Optimized);

    var i: usize = 0;
    while (i < cube_len) : (i += 1) {
        var slow_down = false;
        // x-axies colision
        switch (collides_wall(cubes_x[i], cubes_size[i], width)) {
            1 => {
                slow_down = true;
                cubes_vx[i] *= -1;
            },
            2 => {
                slow_down = true;
                cubes_vx[i] *= -1;
            },
            else => {} 
        }

        // y-axies colision
        switch (collides_ground(cubes_y[i], cubes_size[i], height)) {
            1 => {
                slow_down = true;
                cubes_vy[i] *= -1;
            },
            2 => {
                slow_down = true;
                cubes_vy[i] *= -1;
            },
            else => {}
        }

        // Check if next point is outside bounds
        var cubes_x2 = cubes_x[i];
        var cubes_y2 = cubes_y[i];
        cubes_x[i] += cubes_vx[i];
        cubes_y[i] += cubes_vy[i];

        // Slown down on colision
        if (slow_down) {
            cubes_vx[i] *= 0.9;
            cubes_vy[i] *= 0.9;
            cubes_x2 += cubes_vx[i];
            cubes_y2 += cubes_vy[i];
        }

        cubes_vy[i] -= g * delta;
        cubes_color[i] = hsv_to_rgb(abs(cubes_vy[i]) * 20, 1, 1);
    }
}

fn hsv_to_rgb(h: f32, s: f32, v: f32) u32 {
    @setFloatMode(.Optimized);
    @setRuntimeSafety(false); // Must be off for casting to work 
    const c: f32 = s * v;
    const x: f32 = c * (1 - abs(@mod(h / 60, 2) - 1));
    const m: f32 = v - c;
    var r: u32 = 0;
    var g: u32 = 0;
    var b: u32 = 0;

    switch (@floatToInt(u32, (h / 60)) % 5) {
        0 => {
            r = @floatToInt(u32, (c + m) * 255);
            g = @floatToInt(u32, (x + m) * 255);
            b = 0;
        },
        1 => {
            r = @floatToInt(u32, (x + m) * 255);
            g = @floatToInt(u32, (c + m) * 255);
            b = @floatToInt(u32, (0 + m) * 255);
        },
        2 => {
            r = @floatToInt(u32, (0 + m) * 255);
            g = @floatToInt(u32, (c + m) * 255);
            b = @floatToInt(u32, (x + m) * 255);
        },
        3 => {
            r = @floatToInt(u32, (0 + m) * 255);
            g = @floatToInt(u32, (x + m) * 255);
            b = @floatToInt(u32, (c + m) * 255);
        },
        4 => {
            r = @floatToInt(u32, (x + m) * 255);
            g = @floatToInt(u32, (0 + m) * 255);
            b = @floatToInt(u32, (c + m) * 255);
        },
        5 => {
            r = @floatToInt(u32, (c + m) * 255);
            g = @floatToInt(u32, 0 + m) * 255;
            b = @floatToInt(u32, x + m) * 255;
        },
        else => {
            r = 0xFF;
            g = 0x0F;
            b = 0xF0;
        }
    }

    return (r << (8 * 2)) + (g << (8)) + b;
}

inline fn abs(n: f32) f32 {
    if (n < 0) {
        return n * -1;
    }
    return n;
}
