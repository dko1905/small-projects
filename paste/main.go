package main

import (
	"embed"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/html/v2"
)

//go:embed views/*
var viewsfs embed.FS

func main() {
	engine := html.NewFileSystem(http.FS(viewsfs), ".hbs")
	app := fiber.New(fiber.Config{Views: engine})

	pastes := []Paste{
		{Id: "Alpha", Content: "ABC"},
		{Id: "1984", Content: "The book"},
		{Id: "idk", Content: "ASDJHGASJDHGASD"},
	}

	// Static files
	app.Static("/static", "./static")

	// Views
	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("views/index", fiber.Map{})
	})

	app.Get("/recent", func(c *fiber.Ctx) error {
		return c.Render("views/recent", fiber.Map{
			"Pastes": pastes,
		})
	})

	// Snippets
	app.Get("/98426dd257af461f830098ac1443ae20", func(c *fiber.Ctx) error {
		data := fiber.Map{
			"Name": "Daniel",
		}
		return c.Render("views/98426dd257af461f830098ac1443ae20", data)
	})

	app.Listen(":3000")
}

type Paste struct {
	Id      string
	Content string
}
