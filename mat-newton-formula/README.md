# mat-newton-formula
Simple program for predicting the next number in a number series, using
Newtons formula.

## Use
Run the python script using `python newton.py`.

## More info
Maybe [this](https://www.geeksforgeeks.org/newtons-divided-difference-interpolation-formula/),
I'm not quite sure.

## License
[BSD-3-Clause](./LICENSE)
