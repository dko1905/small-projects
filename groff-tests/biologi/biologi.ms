\# Global options and macros
.EQ
delim $$
.EN
.de BL
.IP \(bu 2
..
.OH 'CC BY 4.0''Copyright (c) 2020, Daniel Florescu'
.EH 'CC BY 4.0''Copyright (c) 2020, Daniel Florescu'
.OF '''side %'
.EF '''side %'
\# Title
.TL
Biologi Noter
.AU
Daniel Florescu & Rasmus Peter Bjørn Whitehorn
.AI
Køge Private Realskole
\# Start of document
.NH
Forord
.NH 2
Licens
.LP
Dette dokument er licenseret under Kreditering 4.0 International (CC BY 4.0),
som kan findes på https://creativecommons.org/licenses/by/4.0/.
.NH 2
Donationer
.LP
Hvis man godt kan lide hvad man ser kan man donere via MobilePay til
: +45 26 85 83 67
.NH
Forurening

.NH 2
Forurening med næringsstoffer (NPK)
.LP
$NO sub x$ oxider er farlige fordi de kan danne syreregn. De danner salpetersyre.
Her er et exempel med $NO sub 3$:
.EQ
NO sub 3 + H sub 2 O => HNO sub 3 + OH sup -
.EN
.LP
Syreregn kan skade planter, ved bladene, roden og jorden omkring, og havdyr.
Nåletræer taget mest skade af sur nedbør, fordi deres nåle skal holde i flere år.
Den sure regn udvasker næringsstoffer som nitrat. Syren kan spalte kemiske
forbindelser, så metaller, som fx. aluminium, optræder som frie ioner i jordens vand.
Metal-ionerne er giftige for planten og kan skade rødderne.

.NH 2
Forurening med svovloxider
.LP
En anden måde at skabe syreregn er med svovloxider.
$SO sub x$ kan reagere med vand og producere svovlsyre.
.EQ
SO sub 4 sup 2- + H sub 2 O -> H sub 2 SO sub 4 + O sup 2-
.EN

.NH 2
Lydfourening
.LP
Når dyr udsættes for støjforurening, vil de forsøge at komme væk.
Hvis de ikke kan komme væk, bliver de stresset.

.NH 2
Partikelforurening
.LP
En anden type forurening er partikelforurening, partikelforurening er små partikler i luften.
Partikelforurening kommer fra ufuldstændig forbrænding, i fx. kulkraftværk er der
ufuldstændig forbrænding, det producere små partikler flyver rundt i luften.
Partikelforurening er rigtig farligt for mennesker, fordi det kan forhøje risikoen for kræft.
Fun fact, der er i København 32 millioner partikler pr. kubikmillimeter luft, i myldretiden.

.NH 2
Fagord
.LP
.B "deponeret ting"
 Ting der ikke kan brændes eller genbruges, deponeres, Dvs. det opbevares.
Fx. asbest og tagplader.

.B "sur nedbør"
 Regn med lav pH.

.NH
Drivhuseffekten
.LP
Drivhuseffekten skyldes et lag i atmosfæren som reflektere kortbølget lys.
Når solens kortbølget lys rammer jorden, bliver den absorberet, jorden mister
så varmen via IR lys, aka kortbølget lys. Drivhusgasserne reflektere det IR lys
tilbage til jorden, så den holder varmen inde.
.PSPIC images/drivhus1.ps 150px


.NH
Genetik
.NH 2
Aminosyrer
.LP
En Aminosyre har formlen $H sub 4 O sub 2 C sub 2 NR$ og et ukendt element, med navnet $R$.
Proteiner er opbygget af flere aminosyrer.
.PSPIC images/amino1.ps 150px

\# Table of contents
.TC
