use finl_unicode::grapheme_clusters::{self, Graphemes};
use std::str;

static STR1_RAW: &'static [u8] = &[
    0xf0, 0x9f, 0x8f, 0x8a, 0xf0, 0x9f, 0x8f, 0xbf, 0xe2, 0x80, 0x8d, 0xe2, 0x99, 0x80, 0xef, 0xb8,
    0x8f, 0x69,
];

fn test1() -> Result<(), Box<dyn std::error::Error>> {
    let str1 = str::from_utf8(STR1_RAW)?.to_owned();
    println!("Str: {}", str1);
    println!("Byte len: {}", str1.len());
    println!("Char len: {}", str1.chars().count());
    for c in str1.chars() {
        print!(" \"{}\" ", c);
    }
    println!("");
    println!("Clusters: {}", Graphemes::new(&str1).count());
    let default_int: u8 = 255;
    for c in Graphemes::new(&str1) {
        let last_byte = c.as_bytes().last().unwrap_or(&default_int);
        print!(" \"{}\"({}) ", c, last_byte);
    }
    println!("");
    Ok(())
}

fn main() {
    match test1() {
        Ok(_) => (),
        Err(e) => {
            panic!("Error: {}", e)
        }
    }
}
