# zgrapheme

Small [zig] 0.10 library for grapheme segmentation. Compliant with unicode
15.0.0 specifictaion.

## Usage

Add this line to your `build.zig`:

```zig
exe.addPackagePath("zgrapheme", "path/to/zgrapheme/zgrapheme.zig");
```

You can then import it like so:

```zig
const zgrapheme = @import("zgrapheme");
```

See [example]/ for some code use cases.

## Contributing

See [CONTRIBUTING.md]

## License

zgrapheme is licensed under the [Mozilla Public License 2.0]

Unicode data is licensed under [UNICODE, INC. LICENSE AGREEMENT]

[zig]: https://ziglang.org/download/
[example]: https://git.sr.ht/~novakane/zgrapheme/tree/main/item/example
[contributing.md]: CONTRIBUTING.md
[mozilla public license 2.0]: COPYING
[unicode, inc. license agreement]: https://www.unicode.org/license.txt
