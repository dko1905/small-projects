// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Compliant with unicode 15.0.0 specifictaion.

/// Iterate over a UTF-8 encoded string.
/// Return a '[]const u8' payload for each grapheme.
/// Return null when no grapheme are found.
pub const Utf8Iterator = @import("lib/grapheme.zig").Utf8Iterator;
/// Iterate over a codepoint array.
/// Return a '[]const u21' payload for each grapheme.
/// Return null when no grapheme are found.
pub const CodepointIterator = @import("lib/grapheme.zig").CodepointIterator;

/// Return the display width of a UTF-8 encoded string.
pub const utf8Width = @import("lib/width.zig").utf8_width;
/// Return the display width of a codepoint.
pub const codepointWidth = @import("lib/width.zig").codepoint_width;
