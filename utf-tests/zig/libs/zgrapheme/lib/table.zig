// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const fmt = std.fmt;
const fs = std.fs;
const io = std.io;
const mem = std.mem;

pub const Range = struct {
    lo: u21,
    up: u21,

    pub fn parse(s: []const u8) !Range {
        var range: Range = undefined;

        if (mem.indexOf(u8, s, "..")) |index| {
            // Input has the form "XXXXXX..XXXXXX"
            range.lo = try fmt.parseUnsigned(u21, s[0..index], 16);
            range.up = try fmt.parseUnsigned(u21, s[index + 2 ..], 16);
        } else {
            // Input has the form "XXXXXX"
            range.lo = try fmt.parseUnsigned(u21, s[0..], 16);
            range.up = range.lo;
        }

        return range;
    }

    fn print(range: Range, writer: anytype) !void {
        try writer.writeAll(".{ .lo = ");
        try writer.print("0x{x}", .{range.lo});
        try writer.writeAll(", .up = ");
        try writer.print("0x{x}", .{range.up});
        try writer.writeAll(" },");
    }
};

/// Binary search in a table of range.
pub fn search(table: []const Range, key: u21) bool {
    var first: usize = 0;
    var last: usize = table.len - 1;

    if (key < table[first].lo or key > table[last].up) {
        return false;
    }

    while (first <= last) {
        var mid = first + ((last - first) >> 1);
        if (key < table[mid].lo) {
            last = mid - 1;
        } else if (key > table[mid].up) {
            first = mid + 1;
        } else {
            return true;
        }
    }

    return false;
}

pub fn print(file: fs.File, name: anytype, list: std.ArrayListUnmanaged(Range)) !void {
    var bw = io.bufferedWriter(file.writer());
    const writer = bw.writer();

    try writer.print("pub const {s} = [_]table.Range{{\n", .{@tagName(name)});
    try writer.writeAll("    ");

    var i: usize = 0;
    for (list.items) |range| {
        i += 1;
        if (i == 3) {
            try range.print(writer);
            try writer.writeAll("\n    ");
            i = 0;
        } else {
            try range.print(writer);
            try writer.writeAll(" ");
        }
    }
    try writer.writeAll("};\n\n");

    try bw.flush();
}
