// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const unicode = std.unicode;

const CodePoint = @This();

/// Offset in bytes.
offset: usize,
cp: u21,

pub const Iterator = struct {
    bytes: []const u8,
    i: usize = 0,

    pub fn init(s: []const u8) !Iterator {
        if (!unicode.utf8ValidateSlice(s)) return error.InvalidUtf8;
        return Iterator{ .bytes = s };
    }

    /// Return the next CodePoint in a UTF-8 encoded string or null.
    pub fn next(it: *Iterator) ?CodePoint {
        if (it.i >= it.bytes.len) return null;

        var codepoint = CodePoint{
            .offset = it.i,
            .cp = undefined,
        };

        const cp_len = unicode.utf8ByteSequenceLength(it.bytes[it.i]) catch unreachable;
        it.i += cp_len;

        codepoint.offset = it.i;
        const bytes = it.bytes[it.i - cp_len .. it.i];
        codepoint.cp = switch (bytes.len) {
            1 => @as(u21, bytes[0]),
            2 => unicode.utf8Decode2(bytes) catch unreachable,
            3 => unicode.utf8Decode3(bytes) catch unreachable,
            4 => unicode.utf8Decode4(bytes) catch unreachable,
            else => unreachable,
        };

        return codepoint;
    }

    /// Look ahead at the next Codepoint without advancing the iterator.
    pub fn peek(it: *Iterator) ?CodePoint {
        const original_i = it.i;
        defer it.i = original_i;
        return if (it.next()) |codepoint| codepoint else null;
    }
};
