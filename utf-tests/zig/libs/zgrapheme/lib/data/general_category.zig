// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const fs = std.fs;
const io = std.io;
const mem = std.mem;

const table = @import("../table.zig");
const tokenizer = @import("tokenizer.zig");

const file_name_in: []const u8 = "DerivedGeneralCategory.txt";
const file_name_out: []const u8 = "general_category.zig";

const Props = enum(u8) {
    uppercase_letter,
    lowercase_letter,
    titlecase_letter,

    modifier_letter,
    other_letter,

    nonspacing_mark,
    spacing_mark,
    enclosing_mark,

    decimal_number,
    letter_number,
    other_number,

    connector_punctuation,
    dash_punctuation,
    open_punctuation,
    close_punctuation,
    initial_punctuation,
    final_punctuation,
    other_punctuation,

    math_symbol,
    currency_symbol,
    modifier_symbol,
    other_symbol,

    space_separator,
    line_separator,
    paragraph_separator,

    control,
    format,
    surrogate,
    private_use,
    unassigned,
};

fn get_prop_int(prop: Props) u8 {
    return @enumToInt(prop);
}

var List = std.enums.directEnumArrayDefault(Props, std.ArrayListUnmanaged(table.Range), .{}, 0, .{});

pub fn print(ally: mem.Allocator, in_dir: fs.Dir, out_dir: fs.Dir) !void {
    const in = try in_dir.openFile(file_name_in, .{});
    const out = try out_dir.createFile(file_name_out, .{});

    try parse(ally, in);

    try out.writeAll("const table = @import(\"../table.zig\");\n\n");
    for (List) |prop, i| {
        try table.print(out, @intToEnum(Props, i), prop);
    }
}

fn parse(ally: mem.Allocator, file: fs.File) !void {
    var br = io.bufferedReader(file.reader());
    var it = tokenizer.tokenize(br.reader());
    var line: usize = 0;
    while (try it.next(&line)) |tok| {
        const r = try table.Range.parse(tok.range);

        if (mem.eql(u8, tok.abbr, "Lu")) {
            try List[get_prop_int(.uppercase_letter)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Ll")) {
            try List[get_prop_int(.lowercase_letter)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Lt")) {
            try List[get_prop_int(.titlecase_letter)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Lm")) {
            try List[get_prop_int(.modifier_letter)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Lo")) {
            try List[get_prop_int(.other_letter)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Mn")) {
            try List[get_prop_int(.nonspacing_mark)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Mc")) {
            try List[get_prop_int(.spacing_mark)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Me")) {
            try List[get_prop_int(.enclosing_mark)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Nd")) {
            try List[get_prop_int(.decimal_number)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Nl")) {
            try List[get_prop_int(.letter_number)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "No")) {
            try List[get_prop_int(.other_number)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Pc")) {
            try List[get_prop_int(.connector_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Pd")) {
            try List[get_prop_int(.dash_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Ps")) {
            try List[get_prop_int(.open_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Pe")) {
            try List[get_prop_int(.close_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Pi")) {
            try List[get_prop_int(.initial_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Pf")) {
            try List[get_prop_int(.final_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Po")) {
            try List[get_prop_int(.other_punctuation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Sm")) {
            try List[get_prop_int(.math_symbol)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Sc")) {
            try List[get_prop_int(.currency_symbol)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Sk")) {
            try List[get_prop_int(.modifier_symbol)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "So")) {
            try List[get_prop_int(.other_symbol)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Zs")) {
            try List[get_prop_int(.space_separator)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Zl")) {
            try List[get_prop_int(.line_separator)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Zp")) {
            try List[get_prop_int(.paragraph_separator)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Cc")) {
            try List[get_prop_int(.control)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Cf")) {
            try List[get_prop_int(.format)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Cs")) {
            try List[get_prop_int(.surrogate)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Co")) {
            try List[get_prop_int(.private_use)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Cn")) {
            try List[get_prop_int(.unassigned)].append(ally, r);
        } else {
            return error.UnknownProps;
        }
    }
}
