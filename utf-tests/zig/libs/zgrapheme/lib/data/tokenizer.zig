// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const ascii = std.ascii;
const mem = std.mem;

fn Tokenizer(comptime T: type) type {
    return struct {
        const Token = struct {
            range: []const u8,
            abbr: []const u8,
        };

        const Self = @This();

        reader: T,
        buffer: [1024]u8 = undefined,

        pub fn next(self: *Self, line: *usize) !?Token {
            while (true) {
                line.* += 1;

                if (try self.reader.readUntilDelimiterOrEof(&self.buffer, '\n')) |_line| {
                    // Skip empty and comment lines.
                    if (_line.len == 0 or _line[0] == '#') continue;

                    const bytes = mem.trim(u8, _line, &ascii.spaces);

                    const range = range_tok: {
                        if (mem.indexOfAny(u8, bytes, ";")) |end| {
                            break :range_tok bytes[0..end];
                        } else {
                            return error.NoRangeFound;
                        }
                    };

                    const abbr = abbr_tok: {
                        if (mem.indexOfAnyPos(u8, bytes, range.len, "#")) |end| {
                            break :abbr_tok bytes[range.len..end];
                        } else {
                            return error.NoAbbrFound;
                        }
                    };

                    return Token{
                        .range = mem.trim(u8, range, &ascii.spaces),
                        .abbr = mem.trim(u8, mem.trimLeft(u8, abbr, ";"), &ascii.spaces),
                    };
                } else {
                    return null;
                }
            }
        }
    };
}

pub fn tokenize(reader: anytype) Tokenizer(@TypeOf(reader)) {
    return .{ .reader = reader };
}
