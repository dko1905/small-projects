// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const fs = std.fs;
const io = std.io;
const mem = std.mem;

const table = @import("../table.zig");
const tokenizer = @import("tokenizer.zig");

const file_name_in: []const u8 = "emoji-data.txt";
const file_name_out: []const u8 = "emoji.zig";

const Props = enum(u4) {
    emoji,
    emoji_presentation,
    emoji_modifier,
    emoji_modifier_base,
    emoji_component,
    extended_pictographic,
};

fn get_prop_int(prop: Props) u4 {
    return @enumToInt(prop);
}

var List = std.enums.directEnumArrayDefault(Props, std.ArrayListUnmanaged(table.Range), .{}, 0, .{});

pub fn print(ally: mem.Allocator, in_dir: fs.Dir, out_dir: fs.Dir) !void {
    const in = try in_dir.openFile(file_name_in, .{});
    const out = try out_dir.createFile(file_name_out, .{});

    try parse(ally, in);

    try out.writeAll("const table = @import(\"../table.zig\");\n\n");
    for (List) |prop, i| {
        try table.print(out, @intToEnum(Props, i), prop);
    }
}

fn parse(ally: mem.Allocator, file: fs.File) !void {
    var br = io.bufferedReader(file.reader());
    var it = tokenizer.tokenize(br.reader());
    var line: usize = 0;
    while (try it.next(&line)) |tok| {
        const r = try table.Range.parse(tok.range);

        if (mem.eql(u8, tok.abbr, "Emoji")) {
            try List[get_prop_int(.emoji)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Emoji_Presentation")) {
            try List[get_prop_int(.emoji_presentation)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Emoji_Modifier")) {
            try List[get_prop_int(.emoji_modifier)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Emoji_Modifier_Base")) {
            try List[get_prop_int(.emoji_modifier_base)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Emoji_Component")) {
            try List[get_prop_int(.emoji_component)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Extended_Pictographic")) {
            try List[get_prop_int(.extended_pictographic)].append(ally, r);
        } else {
            return error.UnknownProps;
        }
    }
}
