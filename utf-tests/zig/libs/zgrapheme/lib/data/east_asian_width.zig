// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const fs = std.fs;
const io = std.io;
const mem = std.mem;

const table = @import("../table.zig");
const tokenizer = @import("tokenizer.zig");

const file_name_in: []const u8 = "DerivedEastAsianWidth.txt";
const file_name_out: []const u8 = "east_asian_width.zig";

const Props = enum(u4) {
    ambiguous,
    fullwidth,
    halfwidth,
    neutral,
    narrow,
    wide,
};

fn get_prop_int(prop: Props) u4 {
    return @enumToInt(prop);
}

var List = std.enums.directEnumArrayDefault(Props, std.ArrayListUnmanaged(table.Range), .{}, 0, .{});

pub fn print(ally: mem.Allocator, in_dir: fs.Dir, out_dir: fs.Dir) !void {
    const in = try in_dir.openFile(file_name_in, .{});
    const out = try out_dir.createFile(file_name_out, .{});

    try parse(ally, in);

    try out.writeAll("const table = @import(\"../table.zig\");\n\n");
    for (List) |prop, i| {
        try table.print(out, @intToEnum(Props, i), prop);
    }
}

fn parse(ally: mem.Allocator, file: fs.File) !void {
    var br = io.bufferedReader(file.reader());
    var it = tokenizer.tokenize(br.reader());
    var line: usize = 0;
    while (try it.next(&line)) |tok| {
        const r = try table.Range.parse(tok.range);

        if (mem.eql(u8, tok.abbr, "A")) {
            try List[get_prop_int(.ambiguous)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "F")) {
            try List[get_prop_int(.fullwidth)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "H")) {
            try List[get_prop_int(.halfwidth)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "N")) {
            try List[get_prop_int(.neutral)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Na")) {
            try List[get_prop_int(.narrow)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "W")) {
            try List[get_prop_int(.wide)].append(ally, r);
        } else {
            return error.UnknownProps;
        }
    }
}
