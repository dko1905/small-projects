// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const fs = std.fs;
const io = std.io;
const mem = std.mem;

const table = @import("../table.zig");
const tokenizer = @import("tokenizer.zig");

const file_name_in: []const u8 = "GraphemeBreakProperty.txt";
const file_name_out: []const u8 = "grapheme_break_property.zig";

const Props = enum(u4) {
    prepend,
    cr,
    lf,
    control,
    extend,
    regional_indicator,
    spacing_mark,
    hangul_l,
    hangul_v,
    hangul_t,
    hangul_lv,
    hangul_lvt,
    zwj,
};

fn get_prop_int(prop: Props) u4 {
    return @enumToInt(prop);
}

var List = std.enums.directEnumArrayDefault(Props, std.ArrayListUnmanaged(table.Range), .{}, 0, .{});

pub fn print(ally: mem.Allocator, in_dir: fs.Dir, out_dir: fs.Dir) !void {
    const in = try in_dir.openFile(file_name_in, .{});
    const out = try out_dir.createFile(file_name_out, .{});

    try parse(ally, in);

    try out.writeAll("const table = @import(\"../table.zig\");\n\n");
    for (List) |prop, i| {
        try table.print(out, @intToEnum(Props, i), prop);
    }
}

fn parse(ally: mem.Allocator, file: fs.File) !void {
    var br = io.bufferedReader(file.reader());
    var it = tokenizer.tokenize(br.reader());
    var line: usize = 0;
    while (try it.next(&line)) |tok| {
        const r = try table.Range.parse(tok.range);

        if (mem.eql(u8, tok.abbr, "Prepend")) {
            try List[get_prop_int(.prepend)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "CR")) {
            try List[get_prop_int(.cr)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "LF")) {
            try List[get_prop_int(.lf)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Control")) {
            try List[get_prop_int(.control)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Extend")) {
            try List[get_prop_int(.extend)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "Regional_Indicator")) {
            try List[get_prop_int(.regional_indicator)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "SpacingMark")) {
            try List[get_prop_int(.spacing_mark)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "L")) {
            try List[get_prop_int(.hangul_l)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "V")) {
            try List[get_prop_int(.hangul_v)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "T")) {
            try List[get_prop_int(.hangul_t)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "LV")) {
            try List[get_prop_int(.hangul_lv)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "LVT")) {
            try List[get_prop_int(.hangul_lvt)].append(ally, r);
        } else if (mem.eql(u8, tok.abbr, "ZWJ")) {
            try List[get_prop_int(.zwj)].append(ally, r);
        } else {
            return error.UnknownProps;
        }
    }
}
