// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const mem = std.mem;
const unicode = std.unicode;

const CodePoint = @import("CodePoint.zig");
const table = @import("table.zig");

const cats = @import("gen/general_category.zig");
const eaw = @import("gen/east_asian_width.zig");
const emoji = @import("gen/emoji.zig");
const gbp = @import("gen/grapheme_break_property.zig");

/// ref: https://unicode.org/reports/tr29/#Default_Grapheme_Cluster_Table
const Properties = enum(u4) {
    other,
    control,
    cr,
    extend,
    extended_pictographic,
    hangul_l,
    hangul_v,
    hangul_t,
    hangul_lv,
    hangul_lvt,
    lf,
    prepend,
    regional_indicator,
    spacingmark,
    zwj,
    num,
};

fn get_prop(cp: u21) Properties {
    if (table.search(&gbp.control, cp)) return .control;
    if (table.search(&gbp.cr, cp)) return .cr;
    if (table.search(&gbp.extend, cp)) return .extend;
    if (table.search(&gbp.hangul_l, cp)) return .hangul_l;
    if (table.search(&gbp.hangul_v, cp)) return .hangul_v;
    if (table.search(&gbp.hangul_t, cp)) return .hangul_t;
    if (table.search(&gbp.hangul_lv, cp)) return .hangul_lv;
    if (table.search(&gbp.hangul_lvt, cp)) return .hangul_lvt;
    if (table.search(&gbp.lf, cp)) return .lf;
    if (table.search(&gbp.prepend, cp)) return .prepend;
    if (table.search(&gbp.regional_indicator, cp)) return .regional_indicator;
    if (table.search(&gbp.spacing_mark, cp)) return .spacingmark;
    if (table.search(&gbp.zwj, cp)) return .zwj;
    if (table.search(&emoji.extended_pictographic, cp)) return .extended_pictographic;

    return .other;
}

fn get_prop_int(prop: Properties) u4 {
    return @enumToInt(prop);
}

pub const Utf8Iterator = struct {
    bytes: []const u8,
    i: usize = 0,

    pub fn init(bytes: []const u8) Utf8Iterator {
        return Utf8Iterator{ .bytes = bytes };
    }

    pub fn next(self: *Utf8Iterator) !?[]const u8 {
        const ret = try utf8_next_break(self.bytes[self.i..]);

        if (ret > 0) {
            const bytes = self.bytes[self.i .. self.i + ret];
            self.i += ret;
            return bytes;
        } else {
            return null;
        }
    }
};

pub const CodepointIterator = struct {
    bytes: []const u21,
    i: usize = 0,

    pub fn init(bytes: []const u21) CodepointIterator {
        return CodepointIterator{ .bytes = bytes };
    }

    pub fn next(self: *CodepointIterator) !?[]const u21 {
        const ret = try codepoint_next_break(self.bytes[self.i..]);

        if (ret > 0) {
            const bytes = self.bytes[self.i .. self.i + ret];
            self.i += ret;
            return bytes;
        } else {
            return null;
        }
    }
};

const State = struct {
    prop: u5 = 0,
    prop_set: bool = false,
    gb11_flag: bool = false,
    gb12_13_flag: bool = false,
};

/// Return the offset in bytes of the next grapheme cluster break in
/// a UTF-8 encoded string. Return 0 at the end of the string.
fn utf8_next_break(s: []const u8) !usize {
    if (s.len == 0) return 0;

    var offset: usize = 0;
    var state: State = .{};

    var it = try CodePoint.Iterator.init(s);
    while (it.next()) |cp| {
        if (it.peek()) |p| {
            if (!unicode.utf8ValidCodepoint(cp.cp) or !unicode.utf8ValidCodepoint(p.cp)) {
                return error.InvalidCodePoint;
            }

            if (codepoint_is_break(cp.cp, p.cp, &state)) {
                offset = cp.offset;
                break;
            }
        } else {
            offset = cp.offset;
            break;
        }
    }

    return offset;
}

/// Return the offset of the next grapheme cluster break in a codepoints array.
/// Return 0 at the end of the string.
fn codepoint_next_break(s: []const u21) !usize {
    if (s.len == 0) return 0;

    var offset: usize = 0;
    var state: State = .{};

    while (offset < s.len) : (offset += 1) {
        if (offset < s.len - 1) {
            if (codepoint_is_break(s[offset], s[offset + 1], &state)) {
                offset += 1;
                break;
            }
        } else {
            offset += 1;
            break;
        }
    }

    return offset;
}

/// Compare two codepoints to check if we can break between them.
fn codepoint_is_break(cp0: u21, cp1: u21, state: *State) bool {
    var cp0_prop: u5 = undefined;
    var cp1_prop: u5 = undefined;

    if (state.prop_set) {
        cp0_prop = state.prop;
    } else {
        cp0_prop = get_prop_int(get_prop(cp0));
    }
    cp1_prop = get_prop_int(get_prop(cp1));

    // Preserve prop of right codepoint for next iteration
    state.prop = cp1_prop;
    state.prop_set = true;

    // Update flags
    state.gb11_flag = (flag_update_gb11[cp0_prop + get_prop_int(.num) * @boolToInt(state.gb11_flag)] &
        @as(u16, 1) << @intCast(u4, cp1_prop) != 0);
    state.gb12_13_flag = (flag_update_gb12_13[cp0_prop + get_prop_int(.num) * @boolToInt(state.gb12_13_flag)] &
        @as(u16, 1) << @intCast(u4, cp1_prop) != 0);

    // Apply grapheme cluster breaking algorithm (UAX #29)
    const not_break = (dont_break[cp0_prop] & (@as(u16, 1) << @intCast(u4, cp1_prop)) != 0) or
        (dont_break_gb11[cp0_prop + @boolToInt(state.gb11_flag) * get_prop_int(.num)] &
        @as(u16, 1) << @truncate(u4, cp1_prop) != 0) or
        (dont_break_gb12_13[cp0_prop + @boolToInt(state.gb12_13_flag) * get_prop_int(.num)] &
        @as(u16, 1) << @intCast(u4, cp1_prop) != 0);

    // Update or reset flags when we have a break
    if (!not_break) {
        state.gb11_flag = false;
        state.gb12_13_flag = false;
    }

    return !not_break;
}

const GBProp = enum(u5) {
    extended_pictographic = get_prop_int(.extended_pictographic),
    regional_indicator = get_prop_int(.regional_indicator),
    gb11_zwj = @as(u5, get_prop_int(.zwj)) + @as(u5, get_prop_int(.num)),
    gb11_extend = @as(u5, get_prop_int(.extend)) + @as(u5, get_prop_int(.num)),
    gb11_extended_pictographic = @as(u5, get_prop_int(.extended_pictographic)) + @as(u5, get_prop_int(.num)),
    gb12_13 = @as(u5, get_prop_int(.regional_indicator)) + @as(u5, get_prop_int(.num)),
};

/// ref: https://www.unicode.org/reports/tr29/#Grapheme_Cluster_Boundary_Rules
///      https://www.unicode.org/Public/15.0.0/ucd/auxiliary/GraphemeBreakTest.html#table
/// Do not break between a CR and LF. Otherwise, break before and after controls.
///     GB3: CR × LF
///     GB4: (Control | CR | LF) ÷
///     GB5: ÷ (Control | CR | LF)
/// Do not break Hangul syllable sequences.
///     GB6: L × (L | V | LV | LVT)
///     GB7: (LV | V) × (V | T)
///     GB8: (LVT | T) × T
/// Do not break before extending characters or ZWJ.
///     GB9 × (Extend | ZWJ)
/// The GB9a and GB9b rules only apply to extended grapheme clusters:
/// Do not break before SpacingMarks, or after Prepend characters.
///     GB9a: × SpacingMark
///     GB9b: Prepend ×
const dont_break = std.enums.directEnumArrayDefault(Properties, u16, 0, 2, .{
    .other = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .cr = @as(u16, 1) << get_prop_int(.lf), // GB3

    .extend = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .extended_pictographic = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .hangul_l = @as(u16, 1) << get_prop_int(.hangul_l) | // GB6
        @as(u16, 1) << get_prop_int(.hangul_v) | // GB6
        @as(u16, 1) << get_prop_int(.hangul_lv) | // GB6
        @as(u16, 1) << get_prop_int(.hangul_lvt) | // GB6
        @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .hangul_v = @as(u16, 1) << get_prop_int(.hangul_v) | // GB7
        @as(u16, 1) << get_prop_int(.hangul_t) | // GB7
        @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .hangul_t = @as(u16, 1) << get_prop_int(.hangul_t) | // GB8
        @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .hangul_lv = @as(u16, 1) << get_prop_int(.hangul_v) | // GB7
        @as(u16, 1) << get_prop_int(.hangul_t) | // GB7
        @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .hangul_lvt = @as(u16, 1) << get_prop_int(.hangul_t) | // GB8
        @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .prepend = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark) | // GB9a
        (@as(u16, 0xFFFF) &
        ~(@as(u16, 1) << get_prop_int(.cr) |
        @as(u16, 1) << get_prop_int(.lf) |
        @as(u16, 1) << get_prop_int(.control))), // GB9b

    .regional_indicator = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .spacingmark = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

    .zwj = @as(u16, 1) << get_prop_int(.extend) | // GB9
        @as(u16, 1) << get_prop_int(.zwj) | // GB9
        @as(u16, 1) << get_prop_int(.spacingmark), // GB9a

});

/// Do not break within emoji modifier sequences or emoji zwj sequences.
/// GB11: \p{Extended_Pictographic} Extend* ZWJ × \p{Extended_Pictographic}
const dont_break_gb11 = std.enums.directEnumArrayDefault(GBProp, u16, 0, 29, .{
    .gb11_zwj = @as(u16, 1) << get_prop_int(.extended_pictographic),
});

const flag_update_gb11 = std.enums.directEnumArrayDefault(GBProp, u16, 0, 26, .{
    .extended_pictographic = @as(u16, 1) << get_prop_int(.zwj) |
        @as(u16, 1) << get_prop_int(.extend),

    .gb11_zwj = @as(u16, 1) << get_prop_int(.extended_pictographic),

    .gb11_extend = @as(u16, 1) << get_prop_int(.extend) |
        @as(u16, 1) << get_prop_int(.zwj),

    .gb11_extended_pictographic = @as(u16, 1) << get_prop_int(.zwj) |
        @as(u16, 1) << get_prop_int(.extend),
});

/// Do not break within emoji flag sequences. That is, do not break between
/// regional indicator (RI) symbols if there is an odd number of RI characters
/// before the break point.
/// GB12: sot (RI RI)* RI × RI
/// GB13: [^RI] (RI RI)* RI × RI
const dont_break_gb12_13 = std.enums.directEnumArrayDefault(GBProp, u16, 0, 29, .{
    .gb12_13 = @as(u16, 1) << get_prop_int(.regional_indicator),
});

const flag_update_gb12_13 = std.enums.directEnumArrayDefault(GBProp, u16, 0, 29, .{
    .regional_indicator = @as(u16, 1) << get_prop_int(.regional_indicator),
});
