// Copyright (C) 2022 Hugo Machet
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const std = @import("std");
const unicode = std.unicode;

const CodePoint = @import("CodePoint.zig");
const table = @import("table.zig");
const grapheme = @import("grapheme.zig");

const cats = @import("gen/general_category.zig");
const eaw = @import("gen/east_asian_width.zig");
const emoji = @import("gen/emoji.zig");
const gbp = @import("gen/grapheme_break_property.zig");

/// Choose the width of Ambiguous characters.
/// Ambiguous characters occur in East Asian legacy character sets as wide
/// characters, but as narrow (i.e., normal-width) characters in non-East
/// Asian usage.
const AmbiguousWidth = enum(u2) {
    half = 1,
    full = 2,
};

/// Get the display width of a UTF-8 encoded string.
pub fn utf8_width(s: []const u8, ambi_w: AmbiguousWidth) !usize {
    var width: isize = 0;

    var graph_it = grapheme.Utf8Iterator.init(s);
    while (try graph_it.next()) |g| {
        var cp_it = (try unicode.Utf8View.init(g)).iterator();

        while (cp_it.nextCodepoint()) |cp| {
            var w = codepoint_width(cp, ambi_w);
            if (w != 0) {
                if (table.search(&emoji.extended_pictographic, cp)) {
                    if (cp_it.nextCodepoint()) |p| {
                        if (p == 0xFE0E) w = 1;
                    }
                }

                width += w;
                break;
            }
        }
    }

    return if (width > 0) @intCast(usize, width) else 0;
}

/// Get the display width of a codepoint.
/// ref: https://github.com/jecolon/ziglyph/blob/main/src/display_width.zig
pub fn codepoint_width(cp: u21, ambi_w: AmbiguousWidth) i3 {
    if (cp == 0x000 or cp == 0x0005 or cp == 0x0007 or (cp >= 0x000A and cp <= 0x000F)) {
        // C0 control codes.
        return 0;
    } else if (cp == 0x0008 or cp == 0x007F) {
        // backspace and DEL
        return -1;
    } else if (cp == 0x00AD) {
        // soft-hyphen
        return 1;
    } else if (cp == 0x2E3A) {
        // two-em dash
        return 2;
    } else if (cp == 0x2E3B) {
        // three-em dash
        return 3;
    } else if (table.search(&cats.enclosing_mark, cp) or table.search(&cats.nonspacing_mark, cp)) {
        // Combining Marks.
        return 0;
    } else if (table.search(&cats.format, cp) and (!(cp >= 0x0600 and cp <= 0x0605) and
        cp != 0x061C and cp != 0x06DD and cp != 0x08E2))
    {
        // Format except Arabic.
        return 0;
    } else if ((cp >= 0x1160 and cp <= 0x11FF) or (cp >= 0x2060 and cp <= 0x206F) or
        (cp >= 0xFFF0 and cp <= 0xFFF8) or (cp >= 0xE0000 and cp <= 0xE0FFF))
    {
        // Hangul syllable and ignorable.
        return 0;
    } else if ((cp >= 0x3400 and cp <= 0x4DBF) or (cp >= 0x4E00 and cp <= 0x9FFF) or
        (cp >= 0xF900 and cp <= 0xFAFF) or (cp >= 0x20000 and cp <= 0x2FFFD) or
        (cp >= 0x30000 and cp <= 0x3FFFD))
    {
        return 2;
    } else if (table.search(&eaw.wide, cp) or table.search(&eaw.fullwidth, cp)) {
        return 2;
    } else if (table.search(&gbp.regional_indicator, cp)) {
        return 2;
    } else if (table.search(&eaw.ambiguous, cp)) {
        return @enumToInt(ambi_w);
    } else {
        return 1;
    }
}
