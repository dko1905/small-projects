const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();

    // This is only needed when updating unicode data after a new release. The
    // generated files are included in the repo.
    const generate = b.option(bool, "gen-table", "Generate data tables, if needed") orelse false;
    if (generate) generate_tables(b);

    inline for ([_][]const u8{
        "display_width",
        "grapheme_iterator",
        "utf32_segmenter",
    }) |example| {
        const exe = b.addExecutable(example, "example/" ++ example ++ ".zig");
        exe.setTarget(target);
        exe.setBuildMode(mode);
        exe.addPackagePath("zgrapheme", "zgrapheme.zig");
        exe.install();
    }

    {
        const tests = b.addTest("lib/test_main.zig");
        tests.setTarget(target);
        tests.setBuildMode(mode);
        const test_step = b.step("test", "Run all tests");
        test_step.dependOn(&tests.step);
    }
}

fn generate_tables(b: *std.build.Builder) void {
    const eaw = @import("lib/data/east_asian_width.zig");
    const cats = @import("lib/data/general_category.zig");
    const gb = @import("lib/data/grapheme_break_property.zig");
    const emoji = @import("lib/data/emoji.zig");

    var in_dir = std.fs.cwd().openDir("lib/data/", .{}) catch @panic("file not found");
    defer in_dir.close();
    var out_dir = std.fs.cwd().openDir("lib/gen/", .{}) catch @panic("file not found");
    defer out_dir.close();

    eaw.print(b.allocator, in_dir, out_dir) catch @panic("out of memory");
    cats.print(b.allocator, in_dir, out_dir) catch @panic("out of memory");
    gb.print(b.allocator, in_dir, out_dir) catch @panic("out of memory");
    emoji.print(b.allocator, in_dir, out_dir) catch @panic("out of memory");
}
