const std = @import("std");
const io = std.io;
const unicode = std.unicode;

const zgrapheme = @import("zgrapheme");

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const ally = gpa.allocator();

    const str = "hello world <<< 🇫🇷 👨‍👩‍👧‍👦 👩🏿>>>";

    var bw = io.bufferedWriter(io.getStdOut().writer());
    const writer = bw.writer();

    try writer.print("Input: {s}", .{str});
    try writer.writeByte('\n');

    var clusters = try utf32Segmenter(ally, str);
    // We need to free all the returned allocated memory by utf32Segmenter().
    defer {
        for (clusters) |c| ally.free(c);
        ally.free(clusters);
    }

    // Loop over the grapheme clusters and print it.
    // Graphemes clusters are array of u32 code points.
    for (clusters) |cluster| {
        try writer.print("{any}", .{cluster});
        try writer.writeByte('\n');
    }

    try bw.flush();
}

/// Segments a UTF-8 encoded string into multiple arrays of code points.
/// Code points are u32 instead of u21 for ease of use with external libraries
/// using UTF-32 encoded string.
/// Caller must freed returned memory.
pub fn utf32Segmenter(allocator: std.mem.Allocator, s: []const u8) ![][]u32 {
    var clusters = try allocator.alloc([]u32, s.len);
    errdefer allocator.free(clusters);

    var count: usize = 0;
    errdefer {
        var i: usize = 0;
        while (i < count) : (i += 1) {
            allocator.free(clusters[i]);
        }
    }

    var it = zgrapheme.Utf8Iterator.init(s);
    while (try it.next()) |grapheme| {
        const cp_count = try unicode.utf8CountCodepoints(grapheme);
        var cluster = try allocator.alloc(u32, cp_count);

        var i: usize = 0;
        var cluster_it = (try unicode.Utf8View.init(grapheme)).iterator();
        while (cluster_it.nextCodepoint()) |codepoint| : (i += 1) {
            cluster[i] = codepoint;
        }

        clusters[count] = cluster[0..];
        count += 1;
    }

    clusters = try allocator.realloc(clusters, count);

    return clusters[0..];
}
