const std = @import("std");
const io = std.io;

const zgrapheme = @import("zgrapheme");

pub fn main() anyerror!void {
    const str = "hello world <<< 🇫🇷 👨‍👩‍👧‍👦 👩🏿>>>";

    var bw = io.bufferedWriter(io.getStdOut().writer());
    const writer = bw.writer();

    try writer.print("Input: {s}\n", .{str});

    var it = zgrapheme.Utf8Iterator.init(str);
    while (try it.next()) |grapheme| {
        const width = try zgrapheme.utf8Width(grapheme, .half);
        try writer.print("width: {d}, bytes: {s}\n", .{ width, grapheme });
    }

    try bw.flush();
}
