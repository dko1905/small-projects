const std = @import("std");
const io = std.io;

const zgrapheme = @import("zgrapheme");

pub fn main() anyerror!void {
    const str = "hello world <<< 🇫🇷 👨‍👩‍👧‍👦 👩🏿>>>";

    var bw = io.bufferedWriter(io.getStdOut().writer());
    const writer = bw.writer();

    try writer.print("Input: {s}\n", .{str});

    var it = zgrapheme.Utf8Iterator.init(str);
    while (try it.next()) |grapheme| {
        try writer.print("{s}\n", .{grapheme});
    }

    try bw.flush();
}
