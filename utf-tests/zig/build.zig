const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    const exe = b.addExecutable("zig", "src/main.zig");
    exe.addPackagePath("zgrapheme", "libs/zgrapheme/zgrapheme.zig");
    exe.addPackagePath("ziglyph", "libs/ziglyph/src/ziglyph.zig");
    const cow_list = std.build.Pkg{ .name = "cow_list", .source = .{ .path = "libs/zigstr/libs/cow_list/src/main.zig" } };
    const ziglyph = std.build.Pkg{ .name = "ziglyph", .source = .{ .path = "libs/zigstr/libs/ziglyph/src/ziglyph.zig" } };
    const zigstr = std.build.Pkg{
        .name = "zigstr",
        .source = .{ .path = "libs/zigstr/src/Zigstr.zig" },
        .dependencies = &[_]std.build.Pkg{ cow_list, ziglyph },
    };
    exe.addPackage(zigstr);
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_tests = b.addTest("src/main.zig");
    exe_tests.setTarget(target);
    exe_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&exe_tests.step);
}
