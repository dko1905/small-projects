const std = @import("std");
const zgrapheme = @import("zgrapheme");
const ziglyph = @import("ziglyph");
const zigstr = @import("zigstr");

pub fn main() !void {
    // Open stdout
    var stdout = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout);
    var writer = bw.writer();
    // Open stdin
    var stdin = std.io.getStdIn().reader();
    var br = std.io.bufferedReader(stdin);
    var reader = br.reader();
    // Allocator
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = gpa.allocator();

    // Read line from stdin, and print each "char".
    while (true) {
        const maybeLine = try reader.readUntilDelimiterOrEofAlloc(allocator, '\n', 1000);
        if (maybeLine == null) return;
        const line = maybeLine.?;
        defer allocator.free(line);
        
        try writer.print("Line \"{s}\" is {} bytes long\n", .{line, line.len});
        
        // zgrapheme
        try writer.print("zgrapheme:\n", .{});
        zgrapheme_print(writer, line) catch |err| std.debug.print("Zgrapheme: {}", .{err});
        try writer.print("\nziglyph:\n", .{});
        ziglyph_print(writer, line) catch |err| std.debug.print("Ziglyph: {}", .{err});
        try writer.print("\nzigstr:\n", .{});
        zigstr_print(allocator, writer, line) catch |err| std.debug.print("Zigstr: {}", .{err});

        try writer.print("\n", .{});
        try bw.flush();
    }
}

fn zgrapheme_print(writer: anytype, line: []u8) !void {
    var it = zgrapheme.Utf8Iterator.init(line);
    while (try it.next()) |grapheme| {
        try writer.print("'{s}' ", .{grapheme});
    }
}

fn ziglyph_print(writer: anytype, line: []u8) !void {
    var it = try ziglyph.GraphemeIterator.init(line);
    while (it.next()) |grapheme| {
        try writer.print("'{s}' ", .{grapheme.bytes});
    }
}

fn zigstr_print(allocator: anytype, writer: anytype, line: []u8) !void {
    var str = try zigstr.fromOwnedBytes(allocator, line);
    const graphemes = try str.graphemes(allocator);
    defer allocator.free(graphemes);
    for (graphemes) |grapheme| {
        try writer.print("'{s}' ", .{grapheme.bytes});
    }
}
